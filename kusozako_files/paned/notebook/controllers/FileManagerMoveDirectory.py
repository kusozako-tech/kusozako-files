
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import InterModelSignals


class DeltaFileManagerMoveDirectory(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, gio_file = user_data
        if signal != InterModelSignals.FILE_MANAGER_MOVE_DIRECTORY:
            return
        notebook = self._enquiry("delta > notebook")
        current_page_index = notebook.get_current_page()
        current_page = notebook.get_nth_page(current_page_index)
        current_page.move_directory(gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register inter model object", self)
