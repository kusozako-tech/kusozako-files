
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import InterModelSignals


class DeltaTrashBinNew(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != InterModelSignals.TRASH_BIN_NEW:
            return
        self._raise("delta > add trash bin")

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register inter model object", self)
