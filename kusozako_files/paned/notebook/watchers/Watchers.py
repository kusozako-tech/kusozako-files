
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .TabVisibility import DeltaTabVisibility


class EchoWatchers:

    def __init__(self, parent):
        DeltaTabVisibility(parent)
