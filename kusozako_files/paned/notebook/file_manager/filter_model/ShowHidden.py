
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals
from kusozako_files import FileManagerColumnTypes


class DeltaShowHidden(DeltaEntity):

    def get_is_hidden(self, tree_row):
        if self._show_hidden:
            return False
        file_info = tree_row[FileManagerColumnTypes.FILE_INFO]
        return file_info.get_is_hidden()

    def receive_transmission(self, user_data):
        signal, show_hidden = user_data
        if signal == FileManagerSignals.FILTER_TOGGLE_SHOW_HIDDEN:
            self._show_hidden = show_hidden
            self._raise("delta > refilter")

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden = False
        self._raise("delta > register model object", self)
