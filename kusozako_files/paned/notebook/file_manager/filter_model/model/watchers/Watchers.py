
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Counter import DeltaCounter
from .sort_functions.SortFunctions import EchoSortFunctions


class EchoWatchers:

    def __init__(self, parent):
        DeltaCounter(parent)
        EchoSortFunctions(parent)
