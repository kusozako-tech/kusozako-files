
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes as Types
from kusozako_files import FileManagerSignals


class DeltaCounter(DeltaEntity):

    def _count(self, model):
        directories = 0
        files = 0
        for tree_row in model:
            if tree_row[Types.MIME_TYPE] == "inode/directory":
                directories += 1
            else:
                files += 1
        signal_param = directories, files
        param = FileManagerSignals.MODEL_COUNT_CHANGED, signal_param
        self._raise("delta > model signal", param)

    def _timeout(self, model, index):
        if index == self._index:
            self._count(model)
        return GLib.SOURCE_REMOVE

    def _on_changed(self, model, *args):
        self._index += 1
        index = self._index
        GLib.timeout_add(100, self._timeout, model, index)

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
        model = self._enquiry("delta > model")
        model.connect("row-inserted", self._on_changed)
        model.connect("row-deleted", self._on_changed)
