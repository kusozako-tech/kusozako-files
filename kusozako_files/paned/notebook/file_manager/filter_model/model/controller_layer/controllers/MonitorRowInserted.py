
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files.tile.Tile import FoxtrotTile
from kusozako_files.file_properties import FileProperties


class AlfaMonitorRowInserted(AlfaFileManagerController):

    SIGNAL = "define accept signal here"
    HEADER_PROPERTY = "HEADER_TEXT", ("red", "green", "blue", "alpha")

    def _build(self, param):
        directory, file_info, gio_file = param
        properties = FileProperties.get_properties(file_info, gio_file)
        pixbufs = self._tile.get_simple_tile(file_info, gio_file)
        tree_row_data = *param, *properties, *pixbufs, False, 0
        tree_row = self._raise("delta > append tree row", tree_row_data)
        self._pixbuf_reloader.reload(tree_row)

    def _control(self, param):
        _, gio_file, bravo_file, _, directory = param
        if bravo_file is not None:
            print(bravo_file.get_path())
        file_info = gio_file.query_info("*", 0)
        param = directory, file_info, gio_file
        if directory == self._enquiry("delta > current directory"):
            GLib.idle_add(self._build, param)

    def _on_initialize(self):
        self._tile = FoxtrotTile.get_default()
        self._pixbuf_reloader = self._raise(
            "delta > request pixbuf reloader",
            self.HEADER_PROPERTY
            )
