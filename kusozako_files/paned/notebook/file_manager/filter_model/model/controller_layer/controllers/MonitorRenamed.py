
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals
from kusozako_files import PreviewStatus
from kusozako_files import FileManagerColumnTypes as ColumnTypes
from kusozako_files.file_properties import FileProperties

HEADER_PROPERTY = "RENAMED", (62/255, 220/255, 46/255, 0.9)

from kusozako_files import FileStateFlags


class DeltaMonitorRenamed(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MONITOR_RENAMED

    def _replace_tree_row_data(self, old_tree_row, new_gio_file):
        file_info = new_gio_file.query_info("*", 0)
        replacements = FileProperties.get_replacements(file_info, new_gio_file)
        name, tooltip_text, collate_key = replacements
        old_tree_row[ColumnTypes.FILE_INFO] = file_info
        old_tree_row[ColumnTypes.GIO_FILE] = new_gio_file
        old_tree_row[ColumnTypes.FILE_NAME] = name
        old_tree_row[ColumnTypes.TOOLTIP_TEXT] = tooltip_text
        old_tree_row[ColumnTypes.FILE_NAME_COLLATE_KEY] = collate_key

    def _replace_preview(self, old_tree_row):
        preview_status = old_tree_row[ColumnTypes.PREVIEW_STATUS]
        if preview_status != PreviewStatus.NEEDLESS:
            old_tree_row[ColumnTypes.PREVIEW_STATUS] = PreviewStatus.UNLOADED
        self._pixbuf_reloader.reload_full(old_tree_row)

    def _try_remove_from_selection_store(self, tree_row, gio_file):
        if not tree_row[ColumnTypes.SELECTED]:
            return
        path = gio_file.get_path()
        param = FileManagerSignals.SELECTION_FILE_RENAMED, (tree_row, path)
        self._raise("delta > model signal", param)

    def _rename(self, old_tree_row, old_gio_file, new_gio_file):
        if old_tree_row is None:
            # when old_tree_row has been deleted or not created.
            directory = new_gio_file.get_parent().get_path()
            signal_param = None, new_gio_file, None, None, directory
            param = FileManagerSignals.MONITOR_CREATED, signal_param
            self._raise("delta > model signal", param)
        else:
            self._replace_tree_row_data(old_tree_row, new_gio_file)
            self._replace_preview(old_tree_row)
            self._try_remove_from_selection_store(old_tree_row, old_gio_file)

    def _control(self, signal_param):
        _, old_file, new_file, _, _ = signal_param
        old_tree_row = self._enquiry("delta > tree row for gio file", old_file)
        new_tree_row = self._enquiry("delta > tree row for gio file", new_file)
        if new_tree_row is None:
            self._rename(old_tree_row, old_file, new_file)
        else:
            print("saved ?")

    def _on_initialize(self):
        self._pixbuf_reloader = self._raise(
            "delta > request pixbuf reloader",
            HEADER_PROPERTY
            )
