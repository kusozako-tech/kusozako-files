
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import PopoverMenuSignals


COPY_TO_CLIPBOARD = (
    {
        "type": "simple-action",
        "title": _("Copy Path"),
        "message": "delta > popover menu signal",
        "user-data": (PopoverMenuSignals.COPY_PATH, None),
        "close-on-clicked": True
    },
    {
        "type": "simple-action",
        "title": _("Copy URI"),
        "message": "delta > popover menu signal",
        "user-data": (PopoverMenuSignals.COPY_URI, None),
        "close-on-clicked": True
    }
)
