
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from . import ThumbnailSize
from .Positions import DeltaPositions


class DeltaThumbnailBuilder(DeltaEntity):

    def _communicate_async_finished(self, subprocess, task, user_data):
        success = subprocess.communicate_finish(task)
        if success:
            self._raise("delta > thumbnail built", user_data)

    def _delta_call_position(self, position):
        _, output_path = GLib.file_open_tmp("XXXXXX-kusozako-files.png")
        percentage = "{}%".format(position/100)
        command_line = [
            "ffmpegthumbnailer",
            "-i", self._input_path,
            "-o", output_path,
            "-t", percentage,
            "-s", self._size
            ]
        subprocess = Gio.Subprocess.new(command_line, Gio.SubprocessFlags.NONE)
        subprocess.communicate_async(
            None,
            None,
            self._communicate_async_finished,
            (output_path, percentage)
            )

    def __init__(self, parent):
        self._parent = parent
        gio_file = self._enquiry("delta > gio file")
        self._input_path = gio_file.get_path()
        size = ThumbnailSize.get_size(self._input_path)
        if size == 0:
            return
        self._size = str(size)
        DeltaPositions(self)
