
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GdkPixbuf
from libkusozako3.Entity import DeltaEntity
from .thumbnail_builder.ThumbnailBuilder import DeltaThumbnailBuilder
from .surface.Surface import FoxtrotSurface


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_call_thumbnail_built(self, user_data):
        path, percentage = user_data
        pixbuf = GdkPixbuf.Pixbuf.new_from_file(path)
        surface = FoxtrotSurface.new_tile()
        thumbnail = surface.paint_pixbuf(pixbuf)
        tree_row_data = path, percentage, thumbnail, pixbuf
        self.append(tree_row_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(
            self,
            str,
            str,
            GdkPixbuf.Pixbuf,
            GdkPixbuf.Pixbuf
            )
        DeltaThumbnailBuilder(self)
