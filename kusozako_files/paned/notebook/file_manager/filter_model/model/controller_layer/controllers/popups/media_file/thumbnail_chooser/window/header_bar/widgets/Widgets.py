
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CancelButton import DeltaCancelButton
from .Label import DeltaLabel
from .SelectButton import DeltaSelectButton


class EchoWidgets:

    def __init__(self, parent):
        DeltaCancelButton(parent)
        DeltaLabel(parent)
        DeltaSelectButton(parent)
