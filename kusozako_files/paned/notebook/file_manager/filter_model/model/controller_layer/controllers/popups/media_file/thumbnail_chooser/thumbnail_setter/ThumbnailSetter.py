
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


class FoxtrotThumbnailSetter:

    def apply(self, tree_row):
        pixbuf = tree_row[3]
        pixbuf.savev(self._path, "png", [], [])

    def __init__(self, gio_file):
        checksum = GLib.Checksum.new(GLib.ChecksumType.MD5)
        uri = gio_file.get_uri()
        checksum.update(bytes(uri, "utf-8"))
        names = [
            GLib.get_user_cache_dir(),
            "thumbnails",
            "kusozako_normal",
            checksum.get_string()+".png"
            ]
        self._path = GLib.build_filenamev(names)
