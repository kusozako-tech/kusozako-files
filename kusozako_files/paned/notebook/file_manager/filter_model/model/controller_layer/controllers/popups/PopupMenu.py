
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu


class AlfaPopupMenu(DeltaEntity):

    MODEL = "define popover model here"

    def _delta_info_sort_column_id(self):
        model = self._enquiry("delta > model")
        column_id, _ = model.get_sort_column_id()
        return column_id

    def _delta_info_sort_order(self):
        model = self._enquiry("delta > model")
        _, order = model.get_sort_column_id()
        return order

    def _on_initialize(self):
        pass

    def __init__(self, parent):
        self._parent = parent
        self._on_initialize()
        self._popover_menu = DeltaPopoverMenu.new_for_model(self, self.MODEL)
