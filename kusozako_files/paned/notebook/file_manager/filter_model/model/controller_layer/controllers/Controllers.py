
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .MoveDirectory import DeltaMoveDirectory
from .FileFound import DeltaFileFound
from .ThumbnailChanged import DeltaThumbnailChanged
from .SortColumnChanged import DeltaSortColumnChanged
from .SortOrderChanged import DeltaSortOrderChanged
from .SelectionToggle import DeltaSelectionToggle
from .SelectionForceSelect import DeltaSelectionForceSelect
from .MonitorMovedOut import DeltaMonitorMovedOut
from .MonitorDeleted import DeltaMonitorDeleted
from .MonitorCreated import DeltaMonitorCreated
from .MonitorMovedIn import DeltaMovedIn
from .MonitorRenamed import DeltaMonitorRenamed
from .ViewersItemActivated import DeltaViewersItemActivated
from .popups.ViewersPopupItemPopup import DeltaViewersPopupItemPopup
from .popups.ViewersPopupNonItemPopup import DeltaViewersPopupNonItemPopup


class EchoControllers:

    def __init__(self, parent):
        DeltaMoveDirectory(parent)
        DeltaFileFound(parent)
        DeltaThumbnailChanged(parent)
        DeltaSortColumnChanged(parent)
        DeltaSortOrderChanged(parent)
        DeltaSelectionToggle(parent)
        DeltaSelectionForceSelect(parent)
        DeltaMonitorMovedOut(parent)
        DeltaMonitorDeleted(parent)
        DeltaMonitorCreated(parent)
        DeltaMovedIn(parent)
        DeltaMonitorRenamed(parent)
        DeltaViewersItemActivated(parent)
        DeltaViewersPopupItemPopup(parent)
        DeltaViewersPopupNonItemPopup(parent)
