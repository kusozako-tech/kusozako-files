
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals
from kusozako_files import FileManagerColumnTypes


class AlfaRowRemoved(AlfaFileManagerController):

    SIGNAL = "define accept signal here."

    def _try_remove_selection(self, model, tree_row, gio_file):
        if not tree_row[FileManagerColumnTypes.SELECTED]:
            return
        param = FileManagerSignals.SELECTION_FILE_REMOVED, gio_file.get_path()
        self._raise("delta > model signal", param)

    def _control(self, param):
        _, gio_file, _, _, directory = param
        tree_row = self._enquiry("delta > tree row for gio file", gio_file)
        if tree_row is None:
            return
        model = self._enquiry("delta > model")
        self._try_remove_selection(model, tree_row, gio_file)
        tree_iter = tree_row.iter
        model.remove(tree_iter)
