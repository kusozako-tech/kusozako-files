
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CopyUri import DeltaCopyUri
from .CopyPath import DeltaCopyPath


class EchoCopyToClipboard:

    def __init__(self, parent):
        DeltaCopyUri(parent)
        DeltaCopyPath(parent)
