
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals
from kusozako_files import PreviewStatus as Status
from kusozako_files import FileManagerColumnTypes as Types


class DeltaThumbnailChanged(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_THUMBNAIL_CHANGED

    def _control(self, gio_file):
        tree_row = self._enquiry("delta > tree row for gio file", gio_file)
        tree_row[Types.PREVIEW_STATUS] = Status.UNLOADED
        param = FileManagerSignals.VIEWERS_FORCE_RELOAD, None
        self._raise("delta > model signal", param)
