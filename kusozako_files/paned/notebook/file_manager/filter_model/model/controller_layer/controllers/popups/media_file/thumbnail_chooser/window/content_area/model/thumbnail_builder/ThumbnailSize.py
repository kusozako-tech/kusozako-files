
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from pymediainfo import MediaInfo
from kusozako_files import Unit


def _try_get_video_track(input_path):
    media_info = MediaInfo.parse(input_path)
    for track in media_info.tracks:
        if track.track_type == "Video":
            return track
    return None


def _get_thumbnail_size(video_track=None):
    if video_track is None:
        return 0
    edge_length = max(video_track.width, video_track.height)
    scale = Unit.TILE_SIZE/min(video_track.width, video_track.height)
    return edge_length*scale


def get_size(input_path):
    video_track = _try_get_video_track(input_path)
    return _get_thumbnail_size(video_track)
