
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files.file_properties import FileProperties
from kusozako_files import FileManagerSignals
from kusozako_files.tile.Tile import FoxtrotTile


class DeltaFileFound(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_FILE_FOUND

    def _build(self, param):
        directory, file_info, gio_file = param
        properties = FileProperties.get_properties(file_info, gio_file)
        pixbufs = self._tile.get_simple_tile(file_info, gio_file)
        tree_row_data = *param, *properties, *pixbufs, False, 0
        self._raise("delta > append tree row", tree_row_data)

    def _control(self, param):
        directory, file_info, gio_file = param
        if directory == self._enquiry("delta > current directory"):
            GLib.idle_add(self._build, param)

    def _on_initialize(self):
        self._tile = FoxtrotTile.get_default()
