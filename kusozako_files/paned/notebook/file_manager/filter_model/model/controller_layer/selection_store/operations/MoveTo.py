
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from libkusozako3 import SafePath
from kusozako_files import FileManagerSignals
from .FileOperation import AlfaFileOperation

FILE_CHOOSER_MODEL = {
    "type": "select-directory",
    "id": None,
    "title": _("Select Directory"),
    "directory": None,
    "read-write-only": True
    }

COPY_FLAGS = Gio.FileCopyFlags


class DeltaMoveTo(AlfaFileOperation):

    SIGNAL = FileManagerSignals.SELECTION_OPERATION_MOVE_TO
    QUEUE_SIGNAL = FileManagerSignals.SELECTION_CAN_WRITE_CHANGED

    def _create_symbolic_link(self, source_file, destination_file, target):
        if not GLib.path_is_absolute(target):
            target = Gio.File.new_for_path(target).get_path()
        success = destination_file.make_symbolic_link(target)
        if success:
            source_file.delete_async(GLib.PRIORITY_DEFAULT_IDLE)

    def _move(self, source_gio_file, destination_gio_file):
        success = source_gio_file.move(
            destination_gio_file,
            COPY_FLAGS.NOFOLLOW_SYMLINKS | COPY_FLAGS.ALL_METADATA,
            None,       # gio cancellable
            None,       # progress callback
            ("",),      # user data for progress call back
            )
        if not success:
            print("move failed", source_gio_file.get_path())

    def _delta_call_gio_file_queued(self, gio_file):
        names = [self._target_directory, gio_file.get_basename()]
        path = GLib.build_filenamev(names)
        safe_path = SafePath.get_path(path)
        destination_gio_file = Gio.File.new_for_path(safe_path)
        file_info = gio_file.query_info("standard::symlink-target", 0)
        target = file_info.get_symlink_target()
        if target is not None:
            self._create_symbolic_link(gio_file, destination_gio_file, target)
        else:
            self._move(gio_file, destination_gio_file)

    def _get_operation_confirmed_by_user(self):
        gio_file = self._async_queue.selection[0]
        parent_gio_file = gio_file.get_parent()
        FILE_CHOOSER_MODEL["directory"] = parent_gio_file.get_path()
        directory = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        self._target_directory = directory
        return directory is not None
