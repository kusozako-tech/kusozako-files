
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3 import SafePath

COPY_FLAGS = Gio.FileCopyFlags


class FoxtrotFileManeuver:

    @classmethod
    def get_default(cls):
        if "default_instance" not in dir(cls):
            cls.default_instance = cls()
        return cls.default_instance

    def _on_copy_finished(self, gio_file, task, user_data=None):
        success = gio_file.copy_finish(task)
        print("success >", success)

    def _get_destination_file(self, directory, source_path):
        basename = GLib.path_get_basename(source_path)
        unsafe_path = GLib.build_filenamev([directory, basename])
        safe_path = SafePath.get_path(unsafe_path)
        return Gio.File.new_for_path(safe_path)

    def _copy_directory(self, directory, source_path):
        dest_file = self._get_destination_file(directory, source_path)
        dest_file.make_directory(None)
        dest_dir = dest_file.get_path()
        source_file = Gio.File.new_for_path(source_path)
        for file_info in source_file.enumerate_children("*", 0):
            names = [source_path, file_info.get_name()]
            path = GLib.build_filenamev(names)
            self.copy_recursive_async(dest_dir, path)

    def _copy_file(self, directory, source_path):
        source_file = Gio.File.new_for_path(source_path)
        dest_file = self._get_destination_file(directory, source_path)
        source_file.copy_async(
            dest_file,
            COPY_FLAGS.ALL_METADATA | COPY_FLAGS.NOFOLLOW_SYMLINKS,
            GLib.PRIORITY_DEFAULT_IDLE,
            None,       # gio cancellable
            None,       # progress callback
            ("",),      # user data for progress call back
            self._on_copy_finished,
            ("",),      # user data for _on_copy_finished
            )

    def _create_symbolic_link(self, directory, source_path):
        source_file = Gio.File.new_for_path(source_path)
        destination_file = self._get_destination_file(directory, source_path)
        file_info = source_file.query_info("standard::symlink-target", 0)
        target = file_info.get_symlink_target()
        if not GLib.path_is_absolute(target):
            target = Gio.File.new_for_path(target).get_path()
        destination_file.make_symbolic_link(target)

    def copy_recursive_async(self, directory, source_path):
        if GLib.file_test(source_path, GLib.FileTest.IS_SYMLINK):
            self._create_symbolic_link(directory, source_path)
        elif GLib.file_test(source_path, GLib.FileTest.IS_REGULAR):
            self._copy_file(directory, source_path)
        else:
            self._copy_directory(directory, source_path)
