
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from .Selection import AlfaSelection


class DeltaCanTrashSelection(AlfaSelection):

    SIGNAL = FileManagerSignals.SELECTION_CAN_TRASH_CHANGED
    ATTRIBUTE = "access::can-trash"
