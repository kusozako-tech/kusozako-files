
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog
from kusozako_files import FileManagerSignals
from .FileOperation import AlfaFileOperation
from .Queue import DeltaQueue

DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "dialog-question-symbolic",
    "message": _("Remove selected files ?"),
    "buttons": (_("Cancel"), _("Remove"))
    }


class DeltaTrash(AlfaFileOperation):

    SIGNAL = FileManagerSignals.SELECTION_OPERATION_TRASH
    QUEUE_SIGNAL = FileManagerSignals.SELECTION_CAN_TRASH_CHANGED

    def _try_trash(self, cache):
        gio_file = cache.pop(0)
        gio_file.trash(None)
        if cache:
            GLib.idle_add(self._try_trash, cache)

    def _control(self, param=None):
        cache = self._queue.get_cache()
        if not cache:
            return
        response = DeltaMessageDialog.run_for_model(self, DIALOG_MODEL)
        if response != 0:
            GLib.idle_add(self._try_trash, cache)

    def _on_initialize(self):
        self._queue = DeltaQueue.new_for_signal(self, self.QUEUE_SIGNAL)
