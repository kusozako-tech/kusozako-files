
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaSelectionFileRemoved(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SELECTION_FILE_REMOVED

    def _control(self, path):
        self._raise("delta > remove", path)
