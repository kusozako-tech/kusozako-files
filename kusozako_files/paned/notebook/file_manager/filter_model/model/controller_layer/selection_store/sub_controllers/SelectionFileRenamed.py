
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals
from kusozako_files import FileManagerColumnTypes


class DeltaSelectionFileRenamed(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SELECTION_FILE_RENAMED

    def _control(self, param):
        tree_row, old_path = param
        self._raise("delta > remove", old_path)
        gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        new_path = gio_file.get_path()
        self._raise("delta > append", (tree_row, new_path))
