
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes as ColumnTypes
from .controllers.Controllers import EchoControllers
from .selection_store.SelectionStore import DeltaSelectionStore
from kusozako_files.PixbufReloader import FoxtrotPixbufReloader


class DeltaControllerLayer(DeltaEntity):

    def _delta_call_request_pixbuf_reloader(self, user_data):
        label_text, label_color = user_data
        return FoxtrotPixbufReloader(label_text, label_color)

    def _delta_info_tree_row_for_gio_file(self, gio_file):
        model = self._enquiry("delta > model")
        for tree_row in model:
            target_gio_file = tree_row[ColumnTypes.GIO_FILE]
            if gio_file.equal(target_gio_file):
                return tree_row
        return None

    def _delta_info_tree_row_for_gio_file_2(self, gio_file):
        model = self._enquiry("delta > filter model")
        for tree_row in model:
            target_gio_file = tree_row[ColumnTypes.GIO_FILE]
            if gio_file.equal(target_gio_file):
                return tree_row
        return None

    def __init__(self, parent):
        self._parent = parent
        # keep initialization order to catch selection signal correctly.
        EchoControllers(self)
        DeltaSelectionStore(self)
