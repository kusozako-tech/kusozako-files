
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FilterKeyword import DeltaFilterKeyword
from .ModelLoaded import DeltaModelLoaded


class EchoControllers:

    def __init__(self, parent):
        DeltaFilterKeyword(parent)
        DeltaModelLoaded(parent)
