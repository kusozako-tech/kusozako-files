
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerColumnTypes


class DeltaUnselectAll(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SELECTION_UNSELECT_ALL

    def _queue_selection(self, new_selection):
        if not new_selection:
            return
        for tree_row in new_selection:
            param = FileManagerSignals.SELECTION_ACCEPTED, tree_row
            self._raise("delta > model signal", param)

    def _control(self, param=None):
        new_selection = []
        filter_model = self._enquiry("delta > filter model")
        for tree_row in filter_model:
            if not tree_row[FileManagerColumnTypes.SELECTED]:
                continue
            tree_row[FileManagerColumnTypes.SELECTED] = False
            new_selection.append(tree_row)
        self._queue_selection(new_selection)
