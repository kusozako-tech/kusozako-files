
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from kusozako_files import FileManagerSignals
from .filter_model.FilterModel import DeltaFilterModel
from .page_content.PageContent import DeltaPageContent


class DeltaFileManager(DeltaEntity):

    @classmethod
    def new_for_gio_file(cls, parent, gio_file=None):
        filer = cls(parent)
        if gio_file is None:
            gio_file = Gio.File.new_for_path(GLib.get_home_dir())
        filer.construct(gio_file)

    def _delta_call_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_info_filter_model(self):
        return self._filter_model.filter_model

    def _delta_call_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def construct(self, gio_file):
        self._filter_model = DeltaFilterModel(self)
        DeltaPageContent(self)
        data = FileManagerSignals.MODEL_MOVE_DIRECTORY, gio_file
        self._transmitter.transmit(data)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
