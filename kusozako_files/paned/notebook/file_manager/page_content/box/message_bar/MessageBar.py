
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import Unit
from .label.Label import DeltaLabel
from .CloseButton import DeltaCloseButton


class DeltaMessageBar(Gtk.Box, DeltaEntity):

    def _delta_call_reveal_child(self, reveal):
        self._revealer.set_reveal_child(reveal)

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        self._revealer = Gtk.Revealer()
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, Unit.TOOLBAR_HEIGHT)
        DeltaLabel(self)
        DeltaCloseButton(self)
        self._revealer.set_reveal_child(False)
        self._revealer.add(self)
        self._raise("delta > add to container", self._revealer)
        self._raise("delta > css", (self, "dialog-error"))
