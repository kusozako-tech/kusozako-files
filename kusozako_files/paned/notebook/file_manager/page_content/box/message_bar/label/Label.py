
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .controllers.Controllers import EchoControllers


class DeltaLabel(Gtk.Label, DeltaEntity):

    def _delta_call_label_changed(self, label_text):
        self.set_label(label_text)
        self._raise("delta > reveal child", True)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self, "test", hexpand=True)
        EchoControllers(self)
        self._raise("delta > add to container", self)
