
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import Unit


class DeltaCloseButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > reveal child", False)

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image.new_from_icon_name(
            "window-close-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(
            self,
            # "Close Message",
            image=image,
            relief=Gtk.ReliefStyle.NONE
            )
        self.props.margin = Unit.MARGIN
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "button-error"))
