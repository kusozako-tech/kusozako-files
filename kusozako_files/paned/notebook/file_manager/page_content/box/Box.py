
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals
from .navigation_bar.NavigationBar import DeltaNavigationBar
from .message_bar.MessageBar import DeltaMessageBar
from .stack.Stack import DeltaStack
from .action_bar.ActionBar import DeltaActionBar


class DeltaBox(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def move_directory(self, gio_file):
        # DeltaBox is added to notebook by FileManager(parent object)
        param = FileManagerSignals.MODEL_MOVE_DIRECTORY, gio_file
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        # DeltaBox is added to notebook by FileManager(parent object)
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaNavigationBar(self)
        DeltaMessageBar(self)
        DeltaStack(self)
        DeltaActionBar(self)
