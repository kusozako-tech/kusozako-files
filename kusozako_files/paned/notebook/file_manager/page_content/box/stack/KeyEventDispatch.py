
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

SIGNALS = {
    "Delete": FileManagerSignals.SELECTION_OPERATION_TRASH,
    "Esc": FileManagerSignals.SELECTION_UNSELECT_ALL,
    "Ctrl+C": FileManagerSignals.SELECTION_OPERATION_COPY_TO,
    "Ctrl+X": FileManagerSignals.SELECTION_OPERATION_MOVE_TO,
    }


class DeltaKeyEventDispatch(DeltaEntity):

    def dispatch(self, controller, keyval, keycode, state):
        label = Gtk.accelerator_get_label(keyval, state)
        signal = SIGNALS.get(label, None)
        if signal is None:
            return
        param = signal, None
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
