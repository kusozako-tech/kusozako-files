
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

DELAY = 50


class DeltaVAdjustment(DeltaEntity):

    def _timeout(self, range_, id_):
        if self._id == id_:
            param = FileManagerSignals.MODEL_REQUEST_PREVIEW, range_
            self._raise("delta > model signal", param)
        return GLib.SOURCE_REMOVE

    def _on_changed(self, *args):
        icon_view = self._enquiry("delta > icon view")
        range_ = icon_view.get_visible_range()
        if range_ is None:
            return
        self._id += 1
        id_ = self._id
        GLib.timeout_add(DELAY, self._timeout, range_, id_)

    def __init__(self, parent):
        self._parent = parent
        self._id = 0
        scrolled_window = self._enquiry("delta > scrolled window")
        vadjustment = scrolled_window.get_vadjustment()
        vadjustment.connect("value-changed", self._on_changed)
