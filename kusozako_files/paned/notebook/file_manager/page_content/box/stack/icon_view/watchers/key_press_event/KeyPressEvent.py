
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .ItemKeyBinds import DeltaItemKeyBinds


class DeltaKeyPressEvent(DeltaEntity):

    def _on_key_press(self, widget, event):
        tree_row = self._enquiry("delta > selected tree row")
        if tree_row is not None:
            self._item_key_binds.dispatch(event, tree_row)

    def __init__(self, parent):
        self._parent = parent
        self._item_key_binds = DeltaItemKeyBinds(self)
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect("key-press-event", self._on_key_press)
