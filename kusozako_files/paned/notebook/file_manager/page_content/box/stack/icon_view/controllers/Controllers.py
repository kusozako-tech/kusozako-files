
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ModelLoaded import DeltaModelLoaded
from .ModelCleared import DeltaModelCleared
from .FilterRefiltered import DeltaFilterRefiltered
from .ForceReload import DeltaForceReload


class EchoControllers:

    def __init__(self, parent):
        DeltaModelLoaded(parent)
        DeltaModelCleared(parent)
        DeltaFilterRefiltered(parent)
        DeltaForceReload(parent)
