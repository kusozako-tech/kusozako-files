
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from .controllers.Controllers import EchoControllers
from .watchers.Watchers import EchoWatchers


class DeltaIconView(Gtk.IconView, DeltaEntity):

    def _delta_info_icon_view(self):
        return self

    def _delta_info_scrolled_window(self):
        return self._scrolled_window

    def _delta_info_selected_tree_row(self):
        tree_paths = self.get_selected_items()
        if not tree_paths:
            return None
        model = self.get_model()
        return model[tree_paths[0]]

    def __init__(self, parent):
        self._parent = parent
        self._scrolled_window = Gtk.ScrolledWindow(vexpand=True)
        Gtk.IconView.__init__(
            self,
            model=self._enquiry("delta > filter model"),
            selection_mode=Gtk.SelectionMode.SINGLE
            )
        EchoWatchers(self)
        EchoControllers(self)
        self.set_pixbuf_column(FileManagerColumnTypes.VISIBLE_PIXBUF)
        self._scrolled_window.add(self)
        data = self._scrolled_window, "icon-view"
        self._raise("delta > add to stack named", data)
