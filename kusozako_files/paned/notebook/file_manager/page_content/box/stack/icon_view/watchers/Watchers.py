
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .QueryTooltip import DeltaQueryTooltip
from .VAdjustment import DeltaVAdjustment
from .button_release_event.ButtonReleaseEvent import DeltaButtonReleaseEvent
from .key_press_event.KeyPressEvent import DeltaKeyPressEvent


class EchoWatchers:

    def __init__(self, parent):
        DeltaQueryTooltip(parent)
        DeltaVAdjustment(parent)
        DeltaButtonReleaseEvent(parent)
        DeltaKeyPressEvent(parent)
