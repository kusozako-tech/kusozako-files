
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals


class DeltaItemKeyBinds(DeltaEntity):

    def _try_activate(self, tree_row):
        param = FileManagerSignals.VIEWERS_ITEM_ACTIVATED, tree_row
        self._raise("delta > model signal", param)

    def _toggle_selection(self, tree_row):
        param = FileManagerSignals.SELECTION_TOGGLE, tree_row
        self._raise("delta > model signal", param)

    def dispatch(self, event, tree_row):
        # if "enter"
        if event.keyval == 65293:
            self._try_activate(tree_row)
        # elif "space"
        elif event.keyval == 32:
            self._toggle_selection(tree_row)

    def __init__(self, parent):
        self._parent = parent
