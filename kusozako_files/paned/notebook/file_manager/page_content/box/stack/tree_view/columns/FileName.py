
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from .ExtraFuncs import BravoExtraFuncs


class DeltaFileName(Gtk.TreeViewColumn, DeltaEntity, BravoExtraFuncs):

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(ellipsize=Pango.EllipsizeMode.END)
        Gtk.TreeViewColumn.__init__(
            self,
            title=_("Name"),
            cell_renderer=renderer,
            text=FileManagerColumnTypes.FILE_NAME
            )
        self._set_sort_column(FileManagerColumnTypes.FILE_NAME_COLLATE_KEY)
        self.set_resizable(True)
        self.set_expand(True)
        self._set_stripe2(renderer)
        self._raise("delta > append tree view column", self)
