
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from .columns.Columns import EchoColumns
from .watchers.Watchers import EchoWatchers


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_call_append_tree_view_column(self, tree_view_column):
        self.append_column(tree_view_column)

    def _delta_info_tree_view(self):
        return self

    def _delta_info_scrolled_window(self):
        return self._scrolled_window

    def _delta_info_selected_tree_row(self):
        selection = self.get_selection()
        response = selection.get_selected_rows()
        if response is None:
            return None
        model, tree_paths = response
        return model[tree_paths[0]] if len(tree_paths) > 0 else None

    def _delta_info_stripe_colors(self, is_odd):
        return self._stripe_colors.get_stripe_colors(is_odd)

    def __init__(self, parent):
        self._parent = parent
        self._scrolled_window = Gtk.ScrolledWindow(vexpand=True)
        Gtk.TreeView.__init__(
            self,
            model=self._enquiry("delta > filter model")
            )
        selection = self.get_selection()
        selection.set_mode(Gtk.SelectionMode.SINGLE)
        EchoWatchers(self)
        EchoColumns(self)
        self._scrolled_window.add(self)
        data = self._scrolled_window, "tree-view"
        self._raise("delta > add to stack named", data)
