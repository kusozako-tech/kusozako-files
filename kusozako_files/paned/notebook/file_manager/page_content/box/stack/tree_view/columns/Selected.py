
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_files import FileManagerColumnTypes
from kusozako_files import FileManagerSignals
from .ExtraFuncs import BravoExtraFuncs


class DeltaSelected(Gtk.TreeViewColumn, DeltaEntity, BravoExtraFuncs):

    def _data_func(self, column, renderer, model, tree_iter):
        renderer.set_property("height", Unit(4))
        renderer.set_property("width", Unit(4))
        tree_row = model[tree_iter]
        selected = tree_row[FileManagerColumnTypes.SELECTED]
        icon_name = "object-select-symbolic" if selected else None
        renderer.set_property("icon_name", icon_name)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererPixbuf(xpad=Unit(1))
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer
            )
        self.set_expand(False)
        self._set_sort_column(FileManagerColumnTypes.SELECTED)
        self._set_stripe2(renderer)
        # self.set_cell_data_func(renderer, self._set_stripe)
        self._raise("delta > append tree view column", self)
