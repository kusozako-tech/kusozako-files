
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_files import FileManagerSignals
from .StripeColors import DeltaStripeColors


class BravoExtraFuncs:

    def _on_clicked(self, tree_view_column, column_id):
        param = FileManagerSignals.SORT_COLUMN_CHANGED, column_id
        self._raise("delta > model signal", param)

    def _data_func(self, column, renderer, model, tree_iter):
        pass

    def _cell_data_func(self, column, renderer, model, iter_, stripe_colors):
        self._data_func(column, renderer, model, iter_)
        tree_path = model.get_path(iter_)
        is_even = (tree_path[0] % 2 == 0)
        stripe_colors.set_background(renderer, is_even)
        if type(renderer) == Gtk.CellRendererText:
            stripe_colors.set_foreground(renderer, is_even)

    def _set_stripe2(self, renderer):
        stripe_colors = DeltaStripeColors(self)
        self.set_cell_data_func(renderer, self._cell_data_func, stripe_colors)

    def _set_sort_column(self, column_id):
        self.set_sort_column_id(column_id)
        self.connect("clicked", self._on_clicked, column_id)
