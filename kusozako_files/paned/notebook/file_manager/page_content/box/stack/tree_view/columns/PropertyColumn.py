
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .ExtraFuncs import BravoExtraFuncs


class DeltaPropertyColumn(Gtk.TreeViewColumn, DeltaEntity, BravoExtraFuncs):

    @classmethod
    def new(cls, parent, header_title, column_id, sort_column_id):
        text_column = cls(parent)
        text_column.construct(header_title, column_id, sort_column_id)

    def construct(self, header_title, column_id, sort_column_id):
        renderer = Gtk.CellRendererText(xalign=1)
        Gtk.TreeViewColumn.__init__(
            self,
            title=header_title,
            cell_renderer=renderer,
            text=column_id
            )
        self._set_sort_column(column_id)
        self._set_stripe2(renderer)
        self.set_resizable(True)
        self.set_expand(False)
        self._raise("delta > append tree view column", self)

    def __init__(self, parent):
        self._parent = parent
