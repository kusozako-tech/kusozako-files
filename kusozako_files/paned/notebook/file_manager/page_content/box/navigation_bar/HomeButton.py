
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals


class DeltaHomeButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        gio_file = Gio.File.new_for_path(GLib.get_home_dir())
        data = FileManagerSignals.MODEL_MOVE_DIRECTORY, gio_file
        self._raise("delta > model signal", data)

    def receive_transmission(self, user_data):
        signal, gio_file = user_data
        if signal == FileManagerSignals.MODEL_LOADED:
            self.set_sensitive(gio_file.get_path() != GLib.get_home_dir())

    def __init__(self, parent):
        self._parent = parent
        icon_params = "go-home-symbolic", Gtk.IconSize.SMALL_TOOLBAR
        image = Gtk.Image.new_from_icon_name(*icon_params)
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self.props.tooltip_text = _("Home")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
