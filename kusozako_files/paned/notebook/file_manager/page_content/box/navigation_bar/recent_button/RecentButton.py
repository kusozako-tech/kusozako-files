
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from kusozako_files import FileManagerSignals
from .RecentPaths import DeltaRecentPaths
from .PopoverModel import MODEL


class DeltaRecentButton(Gtk.Button, DeltaEntity):

    def _delta_call_path_selected(self, path):
        gio_file = Gio.File.new_for_path(path)
        data = FileManagerSignals.MODEL_MOVE_DIRECTORY, gio_file
        self._raise("delta > model signal", data)

    def _on_clicked(self, button, popover):
        popover.popup_for_align(button, 0.5, 0.9)

    def _delta_info_recent_paths(self):
        return self._recent_paths.paths

    def __init__(self, parent):
        self._parent = parent
        self._recent_paths = DeltaRecentPaths(self)
        param = "document-open-recent-symbolic", Gtk.IconSize.SMALL_TOOLBAR
        image = Gtk.Image.new_from_icon_name(*param)
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self.props.tooltip_text = _("Recent")
        popover = DeltaPopoverMenu.new_for_model(self, MODEL)
        self.connect("clicked", self._on_clicked, popover)
        self._raise("delta > add to container", self)
