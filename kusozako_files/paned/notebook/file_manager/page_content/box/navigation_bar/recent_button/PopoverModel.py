
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "recent-paths",
            "display-type": "shorten",
            "message": "delta > path selected",
            "user-data": None,
            "query": "delta > recent paths",
            "query-data": None,
            "close-on-clicked": True
        }
    ]
}

MODEL = [MAIN_PAGE]
