
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

MAXIMUM_LENGTH = 20


class DeltaRecentPaths(DeltaEntity):

    def _control(self, gio_file):
        path = gio_file.get_path()
        if path in self._paths:
            duplicated_index = self._paths.index(path)
            del self._paths[duplicated_index]
        self._paths.insert(0, path)
        del self._paths[MAXIMUM_LENGTH:]

    def receive_transmission(self, user_data):
        signal, gio_file = user_data
        if signal == FileManagerSignals.MODEL_LOADED:
            self._control(gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._paths = []
        self._raise("delta > register model object", self)

    @property
    def paths(self):
        return self._paths
