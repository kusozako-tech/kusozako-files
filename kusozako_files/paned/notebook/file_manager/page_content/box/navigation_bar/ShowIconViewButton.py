
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals


class DeltaShowIconViewButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        data = FileManagerSignals.VIEWERS_SWITCH_TO, "icon-view"
        self._raise("delta > model signal", data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileManagerSignals.VIEWERS_SWITCH_TO:
            self.set_sensitive(param != "icon-view")

    def __init__(self, parent):
        self._parent = parent
        icon_params = "view-grid-symbolic", Gtk.IconSize.SMALL_TOOLBAR
        image = Gtk.Image.new_from_icon_name(*icon_params)
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self.props.tooltip_text = _("Icon View")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
