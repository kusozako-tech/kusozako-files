
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Gio
from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController

ICON_NAME = "go-jump-symbolic"
POSITION = Gtk.EntryIconPosition.SECONDARY


class DeltaSecondaryIcon(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_LOADED

    def _on_clicked(self, entry, position, event):
        if position != POSITION:
            return
        gio_file = Gio.File.new_for_path(self._symlink_target)
        data = FileManagerSignals.MODEL_MOVE_DIRECTORY, gio_file
        self._raise("delta > model signal", data)

    def _control(self, gio_file):
        file_info = gio_file.query_info("*", 0)
        is_symlink = file_info.get_is_symlink()
        icon_name = ICON_NAME if is_symlink else None
        entry = self._enquiry("delta > entry")
        entry.set_icon_from_icon_name(POSITION, icon_name)
        symlink_target = file_info.get_symlink_target()
        if symlink_target is not None:
            tooltip_text = "symlink target : {}".format(symlink_target)
            entry.set_icon_tooltip_text(POSITION, tooltip_text)
        entry.set_icon_activatable(POSITION, is_symlink)
        self._symlink_target = symlink_target

    def _on_initialize(self):
        self._symlink_target = None
        entry = self._enquiry("delta > entry")
        entry.connect("icon-press", self._on_clicked)
