
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController

ICON_NAME = "dialog-error-symbolic"
POSITION = Gtk.EntryIconPosition.PRIMARY


class DeltaPrimaryIcon(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_LOADED

    def _control(self, gio_file):
        file_info = gio_file.query_info("*", 0)
        writable = file_info.get_attribute_boolean("access::can-write")
        icon_name = None if writable else ICON_NAME
        entry = self._enquiry("delta > entry")
        entry.set_icon_from_icon_name(POSITION, icon_name)
        entry.set_icon_tooltip_text(POSITION, _("Read Only Directory"))
