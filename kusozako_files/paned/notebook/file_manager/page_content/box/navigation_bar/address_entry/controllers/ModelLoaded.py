
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController


class DeltaModelLoaded(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_LOADED

    def _control(self, gio_file):
        path = gio_file.get_path()
        self._raise("delta > locked", True)
        entry = self._enquiry("delta > entry")
        entry.set_text(path)
        entry.set_position(len(path))
        self._raise("delta > locked", False)
