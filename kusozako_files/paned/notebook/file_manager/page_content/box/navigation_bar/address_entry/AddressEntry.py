
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .controllers.Controllers import EchoColtrollers


class DeltaAddressEntry(Gtk.Entry, DeltaEntity):

    def _delta_info_entry(self):
        return self

    def _delta_info_locked(self):
        return self._locked

    def _delta_call_locked(self, locked):
        self._locked = locked

    def __init__(self, parent):
        self._parent = parent
        self._locked = False
        Gtk.Entry.__init__(self, hexpand=True)
        self.set_alignment(0.01)
        EchoColtrollers(self)
        self._raise("delta > add to container", self)
