
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .PrimaryIcon import DeltaPrimaryIcon
from .SecondaryIcon import DeltaSecondaryIcon
from .ModelLoaded import DeltaModelLoaded
from .EntryTextChanged import DeltaEntryTextChanged


class EchoColtrollers:

    def __init__(self, parent):
        DeltaPrimaryIcon(parent)
        DeltaSecondaryIcon(parent)
        DeltaModelLoaded(parent)
        DeltaEntryTextChanged(parent)
