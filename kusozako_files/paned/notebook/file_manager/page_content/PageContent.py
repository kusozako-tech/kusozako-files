
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .page_label.PageLabel import DeltaPageLabel
from .box.Box import DeltaBox


class DeltaPageContent(DeltaEntity):

    def _delta_call_close_tab(self):
        self._raise("delta > remove page", self._page_content)

    def __init__(self, parent):
        self._parent = parent
        page_label = DeltaPageLabel(self)
        self._page_content = DeltaBox(self)
        self._raise("delta > append page", (self._page_content, page_label))
