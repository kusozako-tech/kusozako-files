
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals


class DeltaLabel(DeltaEntity, Gtk.Label):

    def receive_transmission(self, user_data):
        signal, gio_file = user_data
        if signal != FileManagerSignals.MODEL_MOVE_DIRECTORY:
            return
        self.set_label(gio_file.get_basename())
        self.props.tooltip_text = gio_file.get_path()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
