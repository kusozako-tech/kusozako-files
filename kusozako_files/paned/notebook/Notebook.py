
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .file_manager.FileManager import DeltaFileManager
from .trash_bin.TrashBin import DeltaTrashBin
from .controllers.Controllers import EchoControllers
from .watchers.Watchers import EchoWatchers


class DeltaNotebook(Gtk.Notebook, DeltaEntity):

    def _delta_info_notebook(self):
        return self

    def _delta_call_append_page(self, user_data):
        page_content, page_label = user_data
        position = self.get_current_page()
        page_index = self.insert_page(page_content, page_label, position+1)
        self.set_tab_reorderable(page_content, True)
        self.show_all()
        self.set_current_page(page_index)

    def _delta_call_add_file_manager(self, gio_file=None):
        DeltaFileManager.new_for_gio_file(self, gio_file)

    def _delta_call_add_trash_bin(self):
        for child in self.get_children():
            if "trash_bin" in str(type(child)):
                self.set_current_page(self.page_num(child))
                return
        DeltaTrashBin(self)

    def _delta_call_remove_page(self, page_content):
        self.detach_tab(page_content)
        page_content.destroy()

    def __init__(self, parent):
        self._parent = parent
        Gtk.Notebook.__init__(self, vexpand=True)
        EchoWatchers(self)
        EchoControllers(self)
        self._raise("delta > add to container", self)
