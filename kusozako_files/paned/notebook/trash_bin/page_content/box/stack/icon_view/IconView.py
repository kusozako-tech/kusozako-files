
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from .watchers.Watchers import EchoWatchers
from .controllers.Controllers import EchoControllers


class DeltaIconView(Gtk.IconView, DeltaEntity):

    def _delta_info_icon_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow()
        Gtk.IconView.__init__(
            self,
            model=self._enquiry("delta > filter model")
            )
        self.set_pixbuf_column(FileManagerColumnTypes.VISIBLE_PIXBUF)
        scrolled_window.add(self)
        user_data = scrolled_window, "icon-view"
        self._raise("delta > add to stack named", user_data)
        EchoWatchers(self)
        EchoControllers(self)
