
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

BINDS = {
    65307: FileManagerSignals.SELECTION_UNSELECT_ALL,       # esc
    }


class DeltaNonItemKeyBinds(DeltaEntity):

    def dispatch(self, event):
        signal = BINDS.get(event.keyval, None)
        if signal is not None:
            self._raise("delta > model signal", (signal, None))

    def __init__(self, parent):
        self._parent = parent
