
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .ItemKeyBinds import DeltaItemKeyBinds
from .NonItemKeyBinds import DeltaNonItemKeyBinds


class DeltaKeyPressEvent(DeltaEntity):

    def _on_key_press(self, widget, event):
        tree_row = self._enquiry("delta > selected tree row")
        if tree_row is not None:
            self._item_key_binds.dispatch(event, tree_row)
        self._non_item_key_binds.dispatch(event)

    def __init__(self, parent):
        self._parent = parent
        self._item_key_binds = DeltaItemKeyBinds(self)
        self._non_item_key_binds = DeltaNonItemKeyBinds(self)
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect("key-press-event", self._on_key_press)
