
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals


class DeltaVAdjustment(DeltaEntity):

    def _on_changed(self, *args):
        icon_view = self._enquiry("delta > icon view")
        range_ = icon_view.get_visible_range()
        if range_ is None:
            return
        data = FileManagerSignals.MODEL_REQUEST_PREVIEW, range_
        self._raise("delta > model signal", data)

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        v_adjustment = icon_view.get_vadjustment()
        v_adjustment.connect("value-changed", self._on_changed)
