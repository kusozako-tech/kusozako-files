
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaModelLoaded(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_LOADED

    def _on_loaded(self):
        icon_view = self._enquiry("delta > icon view")
        range_ = icon_view.get_visible_range()
        if range_ is None:
            return GLib.SOURCE_CONTINUE
        data = FileManagerSignals.MODEL_REQUEST_PREVIEW, range_
        self._raise("delta > model signal", data)
        return GLib.SOURCE_REMOVE

    def _control(self, param):
        GLib.timeout_add(10, self._on_loaded)
