
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_files import FileManagerColumnTypes
from kusozako_files import FileManagerSignals


class DeltaSelected(Gtk.TreeViewColumn, DeltaEntity):

    def _data_func(self, column, renderer, model, tree_iter, user_data=None):
        renderer.set_property("height", Unit(4))
        renderer.set_property("width", Unit(4))
        tree_row = model[tree_iter]
        selected = tree_row[FileManagerColumnTypes.SELECTED]
        icon_name = "object-select-symbolic" if selected else None
        renderer.set_property("icon_name", icon_name)
        color = "White" if tree_row.path[0] % 2 == 0 else "LightGrey"
        renderer.set_property("cell-background", color)

    def _on_clicked(self, tree_view_column, sort_column_id):
        signal_param = FileManagerSignals.SORT_COLUMN_CHANGED, sort_column_id
        self._raise("delta > model signal", signal_param)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererPixbuf(xpad=Unit(1))
        Gtk.TreeViewColumn.__init__(
            self,
            title="",
            cell_renderer=renderer
            )
        self.set_expand(False)
        sort_column_id = FileManagerColumnTypes.SELECTED
        self.set_sort_column_id(sort_column_id)
        self.connect("clicked", self._on_clicked, sort_column_id)
        self.set_cell_data_func(renderer, self._data_func)
        self._raise("delta > append tree view column", self)
