
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals


class DeltaPropertyColumn(Gtk.TreeViewColumn, DeltaEntity):

    @classmethod
    def new(cls, parent, header_title, column_id, sort_column_id):
        text_column = cls(parent)
        text_column.construct(header_title, column_id, sort_column_id)

    def _data_func(self, column, renderer, model, tree_iter, user_data=None):
        tree_path = model.get_path(tree_iter)
        color = "White" if tree_path[0] % 2 == 0 else "LightGrey"
        renderer.set_property("cell-background", color)

    def _on_clicked(self, tree_view_column, sort_column_id):
        signal_param = FileManagerSignals.SORT_COLUMN_CHANGED, sort_column_id
        self._raise("delta > model signal", signal_param)

    def construct(self, header_title, column_id, sort_column_id):
        renderer = Gtk.CellRendererText(xalign=1)
        Gtk.TreeViewColumn.__init__(
            self,
            title=header_title,
            cell_renderer=renderer,
            text=column_id
            )
        self.set_sort_column_id(column_id)
        self.set_resizable(True)
        self.set_expand(False)
        self.set_cell_data_func(renderer, self._data_func)
        self.connect("clicked", self._on_clicked, sort_column_id)
        self._raise("delta > append tree view column", self)

    def __init__(self, parent):
        self._parent = parent
