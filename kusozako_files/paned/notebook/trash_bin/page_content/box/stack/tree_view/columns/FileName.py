
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals
from kusozako_files import FileManagerColumnTypes


class DeltaFileName(Gtk.TreeViewColumn, DeltaEntity):

    def _data_func(self, column, renderer, model, tree_iter, user_data=None):
        tree_path = model.get_path(tree_iter)
        color = "White" if tree_path[0] % 2 == 0 else "LightGrey"
        renderer.set_property("cell-background", color)

    def _on_clicked(self, tree_view_column, sort_column_id):
        param = FileManagerSignals.SORT_COLUMN_CHANGED, sort_column_id
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(ellipsize=Pango.EllipsizeMode.END)
        Gtk.TreeViewColumn.__init__(
            self,
            title=_("Name"),
            cell_renderer=renderer,
            text=FileManagerColumnTypes.FILE_NAME
            )
        sort_column_id = FileManagerColumnTypes.FILE_NAME_COLLATE_KEY
        self.set_sort_column_id(sort_column_id)
        self.set_resizable(True)
        self.set_expand(True)
        self.connect("clicked", self._on_clicked, sort_column_id)
        self.set_cell_data_func(renderer, self._data_func)
        self._raise("delta > append tree view column", self)
