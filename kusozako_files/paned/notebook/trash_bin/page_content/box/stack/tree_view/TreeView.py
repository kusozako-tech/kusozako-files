
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from .watchers.Watchers import EchoWatchers
from .columns.Columns import EchoColumns


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_call_append_tree_view_column(self, tree_view_column):
        self.append_column(tree_view_column)

    def _delta_info_tree_view(self):
        return self

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(vexpand=True)
        Gtk.TreeView.__init__(
            self,
            model=self._enquiry("delta > filter model")
            )
        scrolled_window.add(self)
        selection = self.get_selection()
        selection.set_mode(Gtk.SelectionMode.SINGLE)
        EchoColumns(self)
        EchoWatchers(self)
        user_data = scrolled_window, "tree-view"
        self._raise("delta > add to stack named", user_data)
