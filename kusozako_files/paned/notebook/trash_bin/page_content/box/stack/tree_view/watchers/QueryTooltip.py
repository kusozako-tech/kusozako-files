
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes


class DeltaQueryTooltip(DeltaEntity):

    def _on_query_tooltip(self, tree_view, x, y, keyboard_mode, tooltip):
        _, border = tree_view.get_border()
        model = tree_view.get_model()
        try:
            tree_path, _, _, _ = tree_view.get_path_at_pos(x, y-border.top)
        except TypeError:
            # when pointed at header.
            return False
        tree_row = model[tree_path]
        tooltip.set_icon(tree_row[FileManagerColumnTypes.ORIGINAL_PIXBUF])
        tooltip.set_text(tree_row[FileManagerColumnTypes.TOOLTIP_TEXT])
        tree_view.set_tooltip_row(tooltip, tree_path)
        return True

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.props.has_tooltip = True
        tree_view.connect("query-tooltip", self._on_query_tooltip)
