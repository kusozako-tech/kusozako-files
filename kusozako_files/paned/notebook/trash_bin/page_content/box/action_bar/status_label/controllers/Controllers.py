
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ModelCountChanged import DeltaModelCountChanged
from .SelectionCanReadChanged import DeltaSelectionCanReadChanged


class EchoControllers:

    def __init__(self, parent):
        DeltaModelCountChanged(parent)
        DeltaSelectionCanReadChanged(parent)
