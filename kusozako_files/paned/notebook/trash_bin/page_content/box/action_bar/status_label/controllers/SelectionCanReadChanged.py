
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaSelectionCanReadChanged(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SELECTION_CAN_READ_CHANGED

    def _get_text(self, count):
        if count == 0:
            return None
        elif count == 1:
            return _("1 item selected")
        return _("{} items selected").format(count)

    def _control(self, param):
        text = self._get_text(len(param))
        self._raise("delta > selection text changed", text)
