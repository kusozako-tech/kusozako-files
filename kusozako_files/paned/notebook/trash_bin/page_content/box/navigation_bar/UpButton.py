
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

TRASH_DIR = GLib.build_filenamev([GLib.get_user_data_dir(), "Trash", "files"])


class DeltaUpButton(Gtk.Button, DeltaEntity):

    def _get_destination_gio_file(self, gio_file):
        while True:
            if gio_file.query_exists():
                return gio_file
            gio_file = gio_file.get_parent()

    def _on_clicked(self, button):
        if self._destination is None:
            return
        gio_file = self._get_destination_gio_file(self._destination)
        data = FileManagerSignals.MODEL_MOVE_DIRECTORY, gio_file
        self._raise("delta > model signal", data)

    def receive_transmission(self, user_data):
        signal, gio_file = user_data
        if signal != FileManagerSignals.MODEL_LOADED:
            return
        self._destination = gio_file.get_parent()
        self.set_sensitive(gio_file.get_path() != TRASH_DIR)

    def __init__(self, parent):
        self._parent = parent
        self._destination = None
        icon_params = "go-up-symbolic", Gtk.IconSize.SMALL_TOOLBAR
        image = Gtk.Image.new_from_icon_name(*icon_params)
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self.props.tooltip_text = _("Up")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
