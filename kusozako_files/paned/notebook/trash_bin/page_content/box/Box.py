
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .navigation_bar.NavigationBar import DeltaNavigationBar
from .stack.Stack import DeltaStack
from .action_bar.ActionBar import DeltaActionBar


class DeltaBox(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def move_directory(self, gio_file):
        # DeltaBox is added to notebook by FileManager(parent object)
        self._raise("delta > add file manager", gio_file)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(
            self,
            orientation=Gtk.Orientation.VERTICAL,
            vexpand=True,
            )
        DeltaNavigationBar(self)
        DeltaStack(self)
        DeltaActionBar(self)
