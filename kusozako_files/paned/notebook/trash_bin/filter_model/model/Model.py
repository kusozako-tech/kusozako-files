
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from kusozako_files import FileManagerSignals
from kusozako_files.directory.Directory import DeltaDirectory
from .watchers.Watchers import EchoWatchers
from .controller_layer.ControllerLayer import DeltaControllerlayer


class DeltaModel(Gtk.ListStore, DeltaEntity):

    def _delta_info_current_directory(self):
        return self._directory.current_directory

    def _delta_info_model(self):
        return self

    def _delta_call_append_tree_row(self, tree_row_data):
        tree_iter = self.append(tree_row_data)
        return self[tree_iter]

    def _delta_call_move_directory(self, gio_file):
        self.clear()
        param = FileManagerSignals.MODEL_CLEARED, None
        self._raise("delta > model signal", param)
        self._directory.set_gio_file(gio_file)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *FileManagerColumnTypes.TRASH_BIN_TYPES)
        self.set_sort_column_id(
            FileManagerColumnTypes.DELETION_TIME_UNIX,
            Gtk.SortType.DESCENDING
            )
        EchoWatchers(self)
        DeltaControllerlayer(self)
        self._directory = DeltaDirectory(self)
