
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerColumnTypes
from .SortFunction import AlfaSortFunction


class DeltaFileProperty(AlfaSortFunction):

    COLUMN_IDS = (
        FileManagerColumnTypes.FILE_SIZE,
        FileManagerColumnTypes.PERMISSION,
        FileManagerColumnTypes.LAST_MODIFIED_UNIX,
        FileManagerColumnTypes.CONTENT_TYPE
        )

    def _sort_by_name(self, alfa_row, bravo_row, column_id):
        return 1 if alfa_row[column_id] > bravo_row[column_id] else -1
