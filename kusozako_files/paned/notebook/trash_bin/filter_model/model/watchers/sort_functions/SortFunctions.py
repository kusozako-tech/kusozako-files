
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FileName import DeltaFileName
from .FileProperty import DeltaFileProperty


class EchoSortFunctions:

    def __init__(self, parent):
        DeltaFileName(parent)
        DeltaFileProperty(parent)
