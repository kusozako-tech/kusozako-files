
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes as Types


class AlfaSortFunction(DeltaEntity):

    COLUMN_IDS = ()  # define column ids here

    def _sort_by_is_directory(self, alfa_row, bravo_row, order):
        alfa = alfa_row[Types.IS_DIRECTORY]
        bravo = bravo_row[Types.IS_DIRECTORY]
        if alfa == bravo:
            return 0
        order_coefficient = 1 if order == 0 else -1
        return -1*order_coefficient if alfa > bravo else 1*order_coefficient

    def _sort_by_name(self, alfa_row, bravo_row, sort_column_id):
        raise NotImplementedError

    def _sort_func(self, model, alfa_iter, bravo_iter, user_data):
        alfa_row = model[alfa_iter]
        bravo_row = model[bravo_iter]
        column_id, order = model.get_sort_column_id()
        param = alfa_row, bravo_row, order
        sort_by_is_directory = self._sort_by_is_directory(*param)
        if sort_by_is_directory != 0:
            return sort_by_is_directory
        return self._sort_by_name(alfa_row, bravo_row, column_id)

    def __init__(self, parent):
        self._parent = parent
        model = self._enquiry("delta > model")
        for column_id in self.COLUMN_IDS:
            model.set_sort_func(column_id, self._sort_func)
