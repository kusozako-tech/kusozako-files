
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

COPY_TO_CLIPBOARD = (
    {
        "type": "simple-action",
        "title": _("Copy Path"),
        "message": "delta > copy path",
        "user-data": None,
        "close-on-clicked": True
    },
    {
        "type": "simple-action",
        "title": _("Copy URI"),
        "message": "delta > copy uri",
        "user-data": None,
        "close-on-clicked": True
    }
)
