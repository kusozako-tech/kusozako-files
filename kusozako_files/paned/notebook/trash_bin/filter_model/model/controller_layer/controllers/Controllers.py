
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .ModelMoveDirectory import DeltaModelMoveDirectory
from .FileFound import DeltaFileFound
from .MonitorMovedIn import DeltaMovedIn
from .MonitorMovedOut import DeltaMonitorMovedOut
from .MonitorDeleted import DeltaMonitorDeleted
from .SelectionToggle import DeltaSelectionToggle
from .SelectionForceSelect import DeltaSelectionForceSelect
from .ViewersItemActivated import DeltaViewersItemActivated
from .popups.ViewersPopupItemPopup import DeltaViewersPopupItemPopup
from .popups.ViewersPopupNonItemPopup import DeltaViewersPopupNonItemPopup


class EchoControllers:

    def __init__(self, parent):
        DeltaFileFound(parent)
        DeltaModelMoveDirectory(parent)
        DeltaMovedIn(parent)
        DeltaMonitorMovedOut(parent)
        DeltaMonitorDeleted(parent)
        DeltaSelectionToggle(parent)
        DeltaSelectionForceSelect(parent)
        DeltaViewersItemActivated(parent)
        DeltaViewersPopupItemPopup(parent)
        DeltaViewersPopupNonItemPopup(parent)
