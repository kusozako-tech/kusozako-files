
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaModelMoveDirectory(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_MOVE_DIRECTORY

    def _control(self, gio_file=None):
        if gio_file is None:
            gio_file = self._trash_files_gio_file
        current_directory = self._enquiry("delta > current directory")
        if current_directory == gio_file.get_path():
            return
        self._raise("delta > move directory", gio_file)

    def _on_initialize(self):
        names = [GLib.get_user_data_dir(), "Trash", "files"]
        path = GLib.build_filenamev(names)
        self._trash_files_gio_file = Gio.File.new_for_path(path)
