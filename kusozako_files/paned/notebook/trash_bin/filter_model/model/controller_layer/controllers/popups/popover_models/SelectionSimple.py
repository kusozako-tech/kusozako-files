
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals

SELECTION_SIMPLE = {
    "page-name": "selection-simple",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Select All"),
            "message": "delta > model signal",
            "user-data": (FileManagerSignals.SELECTION_SELECT_ALL, None),
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Unelect All"),
            "message": "delta > model signal",
            "user-data": (FileManagerSignals.SELECTION_UNSELECT_ALL, None),
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Invert Selection"),
            "message": "delta > model signal",
            "user-data": (FileManagerSignals.SELECTION_INVERT_SELECTION, None),
            "close-on-clicked": True
        },
    ]
}
