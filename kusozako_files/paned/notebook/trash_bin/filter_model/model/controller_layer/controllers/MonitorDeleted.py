
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from .MonitorRowRemoved import AlfaRowRemoved


class DeltaMonitorDeleted(AlfaRowRemoved):

    SIGNAL = FileManagerSignals.MONITOR_DELETED
