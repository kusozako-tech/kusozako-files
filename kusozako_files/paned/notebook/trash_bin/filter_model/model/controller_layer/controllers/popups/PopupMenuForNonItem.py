
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from kusozako_files import InterModelSignals
from .PopupMenu import AlfaPopupMenu
from .popover_models.NonItemPopoverMenuModel import MODEL


class DeltaPopupMenuForNonItem(AlfaPopupMenu):

    MODEL = MODEL

    def _delta_call_open_in_new_tab(self):
        directory = self._enquiry("delta > current directory")
        gio_file = Gio.File.new_for_path(directory)
        param = InterModelSignals.FILE_MANAGER_NEW, gio_file
        self._raise("delta > inter model signal", param)

    def popup(self, widget, x, y):
        self._popover_menu.popup_for_position(widget, x, y)
