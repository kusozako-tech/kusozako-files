
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals

SELECTION = {
    "page-name": "selection",
    "items": [
        {
            "type": "back-switcher",
            "title": _("Back"),
            "message": "delta > switch stack to",
            "user-data": "main",
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Select After..."),
            "message": "delta > select from",
            "user-data": FileManagerSignals.SELECTION_SELECT_AFTER,
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Unselect After..."),
            "message": "delta > select from",
            "user-data": FileManagerSignals.SELECTION_UNSELECT_AFTER,
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Select Before..."),
            "message": "delta > select from",
            "user-data": FileManagerSignals.SELECTION_SELECT_BEFORE,
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Unselect Before..."),
            "message": "delta > select from",
            "user-data": FileManagerSignals.SELECTION_UNSELECT_BEFORE,
            "close-on-clicked": True
        },
        {
            "type": "separator"
        },
        {
            "type": "simple-action",
            "title": _("Select All"),
            "message": "delta > model signal",
            "user-data": (FileManagerSignals.SELECTION_SELECT_ALL, None),
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Unelect All"),
            "message": "delta > model signal",
            "user-data": (FileManagerSignals.SELECTION_UNSELECT_ALL, None),
            "close-on-clicked": True
        },
        {
            "type": "simple-action",
            "title": _("Invert Selection"),
            "message": "delta > model signal",
            "user-data": (FileManagerSignals.SELECTION_INVERT_SELECTION, None),
            "close-on-clicked": True
        },
    ]
}
