
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SelectionFileRemoved import DeltaSelectionFileRemoved
from .SelectionFileRenamed import DeltaSelectionFileRenamed
from .ModelCleared import DeltaModelCleared
from .SelectionAccepted import DeltaSelectionAccepted


class EchoSubControllers:

    def __init__(self, parent):
        DeltaSelectionFileRemoved(parent)
        DeltaSelectionFileRenamed(parent)
        DeltaModelCleared(parent)
        DeltaSelectionAccepted(parent)
