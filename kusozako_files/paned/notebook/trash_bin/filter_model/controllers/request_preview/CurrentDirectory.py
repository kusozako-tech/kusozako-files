
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController


class DeltaCurrentDirectory(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_MOVE_DIRECTORY

    def match(self, path):
        return True
        # return self._current_directory == path

    def _control(self, gio_file):
        pass
        # self._current_directory = gio_file.get_path()

    def _on_initialize(self):
        self._current_directory = ""
