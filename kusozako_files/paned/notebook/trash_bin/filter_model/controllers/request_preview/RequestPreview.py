
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController
from .Queue import DeltaQueue
from .PreviewSetter import FoxtrotPreviewSetter


class DeltaRequestPreview(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_REQUEST_PREVIEW

    def _delta_call_queue_ready(self, tree_row_queue):
        self._preview_setter.set_preview_async(tree_row_queue)

    def _control(self, range_):
        self._queue.set_queue_async(range_)

    def _on_initialize(self):
        self._preview_setter = FoxtrotPreviewSetter()
        self._queue = DeltaQueue(self)
