
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import Unit


class DeltaBackButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        self._raise("delta > switch stack to", "bookmark")

    def __init__(self, parent):
        self._parent = parent
        image = Gtk.Image.new_from_icon_name(
            "go-previous-symbolic",
            Gtk.IconSize.SMALL_TOOLBAR
            )
        Gtk.Button.__init__(
            self,
            _("Back to Bookmark"),
            image=image,
            relief=Gtk.ReliefStyle.NONE
            )
        self.set_size_request(-1, Unit.TOOLBAR_HEIGHT)
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
