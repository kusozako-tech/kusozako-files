
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import InterModelSignals
from kusozako_files import FileManagerColumnTypes


class DeltaInterModelSignalReceiver(DeltaEntity):

    def receive_transmission(self, user_data):
        signal, tree_row = user_data
        if signal != InterModelSignals.PREVIEW_SHOW_PREVIEW:
            return
        file_info = tree_row[FileManagerColumnTypes.FILE_INFO]
        gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        mime_type = tree_row[FileManagerColumnTypes.MIME_TYPE]
        user_data = file_info, gio_file, mime_type
        self._raise("delta > show preview", user_data)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register inter model object", self)
