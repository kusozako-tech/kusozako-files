
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.previewers_stack.PreviewersStack import DeltaPreviewersStack
from .InterModelSignalReceiver import DeltaInterModelSignalReceiver
from .BackButton import DeltaBackButton


class DeltaPreviewers(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def _delta_call_show_preview(self, user_data):
        # file_info, gio_file, mime_type = user_data
        success = self._previewers_stack.try_show_preview(*user_data)
        if success:
            self._raise("delta > switch stack to", "previewers")
        else:
            self._previewers_fetcher.stop_media()
            self._raise("delta > switch stack to", "bookmark")

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        DeltaInterModelSignalReceiver(self)
        DeltaBackButton(self)
        self._previewers_stack = DeltaPreviewersStack(self)
        self._raise("delta > add to stack named", (self, "previewers"))
