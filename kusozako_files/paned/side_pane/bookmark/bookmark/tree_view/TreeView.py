
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import BookmarkColumnTypes
from .columns.Columns import EchoColumns
from .watchers.Watchers import EchoWatchers


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _delta_info_tree_view(self):
        return self

    def _delta_call_append_tree_view_column(self, column):
        self.append_column(column)

    def __init__(self, parent):
        self._parent = parent
        scrolled_window = Gtk.ScrolledWindow(vexpand=True)
        Gtk.TreeView.__init__(self, model=self._enquiry("delta > model"))
        self.set_headers_visible(False)
        self.set_tooltip_column(BookmarkColumnTypes.TOOLTIP_TEXT)
        scrolled_window.add(self)
        EchoColumns(self)
        EchoWatchers(self)
        self._raise("delta > css", (self, "secondary-theme-color-class"))
        self._raise("delta > add to container", scrolled_window)
