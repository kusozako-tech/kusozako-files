
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .Icon import DeltaIcon
from .Text import DeltaText


class EchoColumns:

    def __init__(self, parent):
        DeltaIcon(parent)
        DeltaText(parent)
