
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import InterModelSignals
from kusozako_files import BookmarkColumnTypes

from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu
from .PopoverModel import POPOVER_MODEL


class DeltaButtonReleaseEvent(DeltaEntity):

    def _get_gio_file(self, tree_view, event):
        model = tree_view.get_model()
        response = tree_view.get_path_at_pos(event.x, event.y)
        if response is None:
            return None
        tree_path, _, _, _ = response
        tree_row = model[tree_path]
        gio_file = tree_row[BookmarkColumnTypes.GIO_FILE]
        if gio_file.get_path() == "/dev/null":
            return None
        return gio_file

    def _delta_call_callback(self, signal):
        param = signal, self._gio_file
        self._raise("delta > inter model signal", param)

    def _on_row_activated(self, tree_view, event, popover_menu):
        if event.button != 3:
            return
        self._gio_file = self._get_gio_file(tree_view, event)
        if self._gio_file is None:
            return
        popover_menu.popup_for_position(tree_view, event.x, event.y)

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        popover_memu = DeltaPopoverMenu.new_for_model(self, POPOVER_MODEL)
        tree_view.connect(
            "button-release-event",
            self._on_row_activated,
            popover_memu
            )
