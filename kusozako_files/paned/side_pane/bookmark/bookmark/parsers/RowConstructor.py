
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3 import HomeDirectory


class BravoRowConstructor:

    def _construct(self, type_, gio_file, icon_name="folder-symbolic"):
        if not gio_file.query_exists():
            return
        gio_icon = Gio.ThemedIcon.new_with_default_fallbacks(icon_name)
        basename = gio_file.get_basename()
        tooltip = HomeDirectory.shorten(gio_file.get_path())
        row_data = (
            gio_icon,
            type_,
            basename,
            tooltip,
            gio_file
            )
        self._raise("delta > append row data", row_data)
