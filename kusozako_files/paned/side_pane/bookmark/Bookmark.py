
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .bookmark.Bookmark import DeltaBokmark as _DeltaBookmark
from .trash_bin.TrashBin import DeltaTrashBin


class DeltaBokmark(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.VERTICAL)
        _DeltaBookmark(self)
        self.add(Gtk.Separator())
        DeltaTrashBin(self)
        self._raise("delta > add to stack named", (self, "bookmark"))
