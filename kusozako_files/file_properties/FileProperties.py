
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FilePermission
from . import Types
from . import Names
from . import Times
from . import Sizes
from . import TrashInfo


def get_properties(file_info, gio_file):
    types = Types.get_types(file_info, gio_file)
    names = Names.get_names(types[0], file_info)
    times = Times.get_times(file_info)
    sizes = Sizes.get_sizes(file_info)
    permissions = FilePermission.get_permission(file_info)
    return *types, *names, *times, *sizes, *permissions


def get_trash_info(gio_file):
    return TrashInfo.get_file_info(gio_file)


def get_replacements(file_info, gio_file):
    types = Types.get_types(file_info, gio_file)
    names = Names.get_names(types[0], file_info)
    return names
