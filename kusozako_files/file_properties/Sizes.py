
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

FORMAT = GLib.FormatSizeFlags.IEC_UNITS


def get_sizes(file_info):
    size = file_info.get_size()
    size_readable = GLib.format_size_full(size, FORMAT)
    return size, size_readable
