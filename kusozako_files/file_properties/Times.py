
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


def get_times(file_info):
    modification_time = file_info.get_modification_date_time().to_local()
    diff = GLib.DateTime.new_now_local().difference(modification_time)
    format_ = "%F" if diff/(GLib.TIME_SPAN_DAY) > 1 else "%T"
    return modification_time.to_unix(), modification_time.format(format_)
