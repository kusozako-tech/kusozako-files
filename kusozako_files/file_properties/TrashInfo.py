
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib

trash_info_names = [GLib.get_user_data_dir(), "Trash", "info"]
TRASH_INFO_DIR = GLib.build_filenamev(trash_info_names)
DEFAULT_TIME_ZONE = GLib.TimeZone.new_local()


def __get_paths(gio_file):
    trash_file_path = gio_file.get_path()
    path_elements = trash_file_path.split("/")
    root_name = path_elements[7]
    root_path = GLib.build_filenamev(path_elements[0:8])
    base_path = trash_file_path.replace(root_path, "")
    names = [TRASH_INFO_DIR, root_name+".trashinfo"]
    file_info_path = GLib.build_filenamev(names)
    return file_info_path, base_path


def __get_times(key_file):
    deletion_date_time = GLib.DateTime.new_from_iso8601(
        key_file.get_string("Trash Info", "DeletionDate"),
        DEFAULT_TIME_ZONE
        )
    diff = GLib.DateTime.new_now_local().difference(deletion_date_time)
    format_ = "%F" if diff/(GLib.TIME_SPAN_DAY) > 1 else "%T"
    return deletion_date_time.to_unix(), deletion_date_time.format(format_)


def get_file_info(gio_file):
    file_info_path, base_path = __get_paths(gio_file)
    key_file = GLib.KeyFile.new()
    key_file.load_from_file(file_info_path, GLib.KeyFileFlags.NONE)
    original_path = key_file.get_string("Trash Info", "Path")
    path = GLib.build_filenamev([original_path, base_path])
    destination, _ = GLib.filename_from_uri("File://"+path)
    unix_time, readable_time = __get_times(key_file)
    return destination, unix_time, readable_time
