
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# signals                            param:
MODEL_CLEARED = 0                   # NONE
MODEL_MOVE_DIRECTORY = 1            # Gio.File for destination directory
MODEL_FILE_FOUND = 2                # str as directory, Gio.FileInfo, Gio.Fie
MODEL_LOADED = 3                    # Gio.File for newly loaded directory
MODEL_REQUEST_PREVIEW = 4           # (tree_path, tree_path) as start and end
MODEL_COUNT_CHANGED = 5             # (int, int) as directories and fies count
MODEL_THUMBNAIL_CHANGED = 6         # gio_file for changed
SORT_COLUMN_CHANGED = 7             # int as sort column id
SORT_ORDER_CHANGED = 8              # int as Gtk.SortType
FILTER_TOGGLE_SHOW_HIDDEN = 9       # bool as show hidden
FILTER_KEYWORD = 10                  # str as filter keyword
FILTER_REFILTERED = 11              # NONE
# NOTE:
# SELECTION_TOGGLE and SELECTION_FORCE_SELECT could be rejected by
# file permission. if it's not rejected, application should raise
# SELECTION_ACCEPTED signal.
SELECTION_TOGGLE = 12               # tree_row of base model
SELECTION_FORCE_SELECT = 13         # tree_row of base model
SELECTION_ACCEPTED = 14             # tree_row of base_model
SELECTION_CAN_READ_CHANGED = 15     # dict{str as path: tree_row}
SELECTION_CAN_WRITE_CHANGED = 16    # dict[str as path: tree_row]
SELECTION_CAN_TRASH_CHANGED = 17    # dict[str as path: tree_row]
SELECTION_OPERATION_COPY_TO = 18    # NONE
SELECTION_OPERATION_MOVE_TO = 19    # NONE
SELECTION_OPERATION_TRASH = 20      # NONE
SELECTION_OPERATION_RESTORE_TRASH = 21  # NONE
SELECTION_OPERATION_DELETE_TRASH = 22   # NONE
SELECTION_FILE_RENAMED = 23         # tree_row, str as OLD path
SELECTION_FILE_REMOVED = 24         # str as path
# NOTE:
# SELECTION_SELECT_ALL to SELECTION_UNSELECT_AFTER will
# work with filter model, not base model.
SELECTION_SELECT_ALL = 25           # NONE
SELECTION_UNSELECT_ALL = 26         # NOEN
SELECTION_INVERT_SELECTION = 27     # NONE
SELECTION_SELECT_BEFORE = 28        # pending
SELECTION_UNSELECT_BEFORE = 29      # pending
SELECTION_SELECT_AFTER = 30         # gio_file as start from...
SELECTION_UNSELECT_AFTER = 31       # gio_file as start from...
# NOTE:
# stack name must be "icon-view" or "tree-view".
VIEWERS_SWITCH_TO = 32              # str as stack name.
VIEWERS_ITEM_ACTIVATED = 33         # tree_row, Item Type not decided yet.
VIEWERS_POPUP_ITEM_POPUP = 34       # tree_row
VIEWERS_POPUP_NON_ITEM_POPUP = 35   # NONE
VIEWERS_FORCE_RELOAD = 36           # NONE
# NOTE:
# all monitor signals has same params as...
# file_monitor, gio_file, gio_file or None, event_type and str as directory
# but secondary gio_file should be used by MONITOR_RENAMED only
MONITOR_CHANGED = 37
MONITOR_CHANGES_DONE = 38
MONITOR_DELETED = 39                # DONE
MONITOR_CREATED = 40                # DONE
MONITOR_ATTRIBUTE_CHANGED = 41
MONITOR_RENAMED = 42                # TODO
MONITOR_MOVED_IN = 43               # DONE
MONITOR_MOVED_OUT = 44              # DONE
# NOTE:
# monitr signals for current directory
# params are same as monitor signals
CURRENT_DIRECTORY_RENAMED = 45
CURRENT_DIRECTORY_REMOVED = 46      # DOING

NUMBER_OF_SIGNALS = 47
