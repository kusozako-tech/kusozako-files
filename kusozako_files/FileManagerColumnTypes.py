
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GObject
from gi.repository import Gio
from gi.repository import GdkPixbuf

DIRECTORY = 0
FILE_INFO = 1
GIO_FILE = 2
MIME_TYPE = 3
CONTENT_TYPE = 4
IS_DIRECTORY = 5
PREVIEW_STATUS = 6
FILE_NAME = 7
TOOLTIP_TEXT = 8
FILE_NAME_COLLATE_KEY = 9
LAST_MODIFIED_UNIX = 10
LAST_MODIFIED_READABLE = 11
FILE_SIZE = 12
FILE_SIZE_READABLE = 13
PERMISSION = 14
PERMISSION_READABLE = 15
ORIGINAL_PIXBUF = 16
SOURCE_PIXBUF = 17
VISIBLE_PIXBUF = 18
SELECTED = 19
STATE_FLAG = 20

NUMBER_OF_COLUMNS = 21

ORIGINAL_PATH = 21
DELETION_TIME_UNIX = 22
DELETION_TIME_READABLE = 23

NUMBER_OF_COLUMNS_TRASH_BIN = 24

# NOTE:
# python type "int" should be converted to GObject.TYPE_INT (32bit),
# that overflows if the value not in range -2147483648 to 2147483647.

TYPES = (
    str,                    # 0: DIRECTORY
    Gio.FileInfo,           # 1: FILE_INFO
    Gio.File,               # 2: GIO_FILE
    str,                    # 3: MIME_TYPE by magic
    str,                    # 4: CONTENT_TYPE by Gio.FileInfo
    bool,                   # 5: IS_DIRECTORY
    int,                    # 6: PREVIEW_STATUS
    str,                    # 7: FILE_NAME
    str,                    # 8: TOOLTIP_TEXT
    str,                    # 9: FILE_NAME_COLLATE_KEY
    int,                    # 10: LAST_MODIFIED
    str,                    # 11: LAST_MODIFIED_READABLE
    GObject.TYPE_ULONG,     # 12: FILE_SIZE
    str,                    # 13: FILE_SIZE_READABLE
    int,                    # 14: PERMISSION
    str,                    # 15: PERMISSION_READABLE
    GdkPixbuf.Pixbuf,       # 16: ORIGINAL_PIXBUF
    GdkPixbuf.Pixbuf,       # 17: SOURCE_PIXBUF
    GdkPixbuf.Pixbuf,       # 18: VISIBLE_PIXBUF
    bool,                   # 19: SELECTED
    int,                    # 20: STATE_FLAG
    )

TRASH_BIN_TYPES = TYPES + (
    str,                    # 21: ORIGINAL_PATH
    GObject.TYPE_ULONG,     # 22: DELETION_TIME_UNIX
    str,                    # 23: DELETION_TIME_READABLE
    )
