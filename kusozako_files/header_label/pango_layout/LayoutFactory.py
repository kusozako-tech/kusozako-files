
from gi.repository import Pango
from gi.repository import PangoCairo
from libkusozako3.Ux import Unit
from libkusozako3.gtk_font import GtkFont

MAXIMUM_TEXT_HEIGHT = Unit(8)
TILE_SIZE = Unit(16)
MARGIN = Unit(0.5)


def get_from_cairo_context(cairo_context):
    layout = PangoCairo.create_layout(cairo_context)
    layout.set_alignment(Pango.Alignment.CENTER)
    layout.set_font_description(GtkFont.get_description(relative_size=-1))
    layout.set_width((TILE_SIZE-MARGIN*2)*Pango.SCALE)
    layout.set_height(MAXIMUM_TEXT_HEIGHT*Pango.SCALE)
    layout.set_wrap(Pango.WrapMode.WORD_CHAR)
    layout.set_ellipsize(Pango.EllipsizeMode.MIDDLE)
    return layout.copy()
