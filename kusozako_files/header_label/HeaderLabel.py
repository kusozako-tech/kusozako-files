
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gdk
from libkusozako3.Ux import Unit
from .pango_layout.PangoLayout import FoxtrotPangoLayout

SIZE = Unit(16), Unit(16)
TILE_SIZE = Unit(16)
MARGIN = Unit(0.5)


class FoxtrotHeaderLabel:

    @classmethod
    def new(cls, label_text, color):
        header_label = cls(label_text, color)
        return header_label

    def _get_label_geometries(self, inner_height):
        outer_height = inner_height+MARGIN*2
        return 0, 0, TILE_SIZE, outer_height

    def _paint_shade(self, cairo_context, height):
        cairo_context.set_source_rgba(*self._shade_color)
        cairo_context.rectangle(*self._get_label_geometries(height))
        cairo_context.fill()

    def _paint(self, cairo_context, title):
        layout = FoxtrotPangoLayout(cairo_context, title)
        _, height = layout.get_pixel_size()
        self._paint_shade(cairo_context, height)
        cairo_context.set_source_rgba(1, 1, 1, 1)
        cairo_context.move_to(MARGIN, MARGIN)
        layout.paint()

    def get_visible_pixbuf(self, pixbuf):
        if isinstance(pixbuf, tuple):
            print("MUST FIX IT, THIS IS SERIOUS BUG")
            pixbuf = pixbuf[0]
        surface = cairo.ImageSurface(cairo.Format.ARGB32, *SIZE)
        cairo_context = cairo.Context(surface)
        Gdk.cairo_set_source_pixbuf(cairo_context, pixbuf, 0, 0)
        cairo_context.paint()
        self._paint(cairo_context, self._label_text)
        return Gdk.pixbuf_get_from_surface(surface, 0, 0, *SIZE)

    def __init__(self, label_text, color):
        self._label_text = label_text
        self._shade_color = color
