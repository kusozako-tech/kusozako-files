
# (c) copyright 2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

NONE = 0
SELECTED = 1
RENAMED = 2
CREATED = 4
MOVED_IN = 8

ORDER = [SELECTED, RENAMED, CREATED, MOVED_IN]


def get_primary_state(current_state):
    for flag in ORDER:
        if current_state & flag != 0:
            return flag
    return NONE


def set_selected(current_state):
    current_state |= SELECTED
    return current_state


def toggle_selected(current_state):
    if current_state & SELECTED != 0:
        current_state -= current_state & SELECTED
    else:
        current_state |= SELECTED
    return current_state


def set_renamed(current_state):
    current_state |= RENAMED
    return current_state


def set_created(current_state):
    current_state |= CREATED
    return current_state
