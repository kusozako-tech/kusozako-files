kusozako-files (2022.04.26) UNRELEASED; urgency=low

  * use main_loop insted of application.
  * fail safe for xdg-user-dirs missing.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Mon, 26 Apr 2022 00:00:00 +0000

kusozako-files (2022.02.19) UNRELEASED; urgency=low

  * improve directory parsing speed.
  * improve trash operation speed
  * new: delete button for trash-bin-page
  * new: restore button for trash-bin-page
  * new: copy button for trash-bin-pages.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Sat, 19 Feb 2022 00:00:00 +0000

kusozako-files (2022.02.18.1) UNRELEASED; urgency=low

  * improve file deletion.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Fri, 18 Feb 2022 00:00:01 +0000

kusozako-files (2022.02.18) UNRELEASED; urgency=low

  * new: tree_view_row stripe colors respect user configured css.
  * new: show target link on tooltip text. if it exists.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Fri, 18 Feb 2022 00:00:00 +0000

kusozako-files (2021.12.25) UNRELEASED; urgency=low

  * move file tile funcs to libkusozako3

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Sat, 25 Dec 2021 00:00:00 +0000

kusozako-files (2021.12.04) UNRELEASED; urgency=low

  * bugfix: fullreload tile on_file_renamed works correctly again.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Sat, 04 Dec 2021 00:00:00 +0000

kusozako-files (2021.12.03) UNRELEASED; urgency=low

  * new: make directry popover menu.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Fri, 03 Dec 2021 00:00:01 +0000

kusozako-files (2021.12.02.1) UNRELEASED; urgency=low

  * bugfix and code refactoring.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Thu, 02 Dec 2021 00:00:01 +0000

kusozako-files (2021.12.02) UNRELEASED; urgency=low

  * new: preview for tooltip

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Thu, 02 Dec 2021 00:00:00 +0000

kusozako-files (2021.11.17) UNRELEASED; urgency=low

  * new: more keybinds.
  * improve .part file handling.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Wed, 17 Nov 2021 00:00:01 +0000

kusozako-files (2021.11.15) UNRELEASED; urgency=low

  * new: popover menu for bookmark pane.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Mon, 15 Nov 2021 00:00:01 +0000

kusozako-files (2021.11.12) UNRELEASED; urgency=low

  * bugfix and code clean-up.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Fri, 12 Nov 2021 00:00:01 +0000

kusozako-files (2021.11.03.1) UNRELEASED; urgency=low

  * use libkusozako.previewers_stack.PreviewersStack instead of local module.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Wed, 03 Nov 2021 00:00:01 +0000

kusozako-files (2021.11.03) UNRELEASED; urgency=low

  * bugfix.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Wed, 03 Nov 2021 00:00:00 +0000

kusozako-files (2021.11.02) UNRELEASED; urgency=low

  * use new non-blocking methods for trash file
  * now can copy symlink correctly

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Tue, 02 Nov 2021 00:00:00 +0000

kusozako-files (2021.10.27) UNRELEASED; urgency=low

  * new: video thumbnail chooser dialog

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Wed, 27 Sep 2021 00:00:00 +0000

kusozako-files (2021.10.26) UNRELEASED; urgency=low

  * use cache for thumbnails

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Tue, 26 Sep 2021 00:00:00 +0000

kusozako-files (2021.10.13) UNRELEASED; urgency=low

  * icon-view: event delay for request-preview

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Tue, 21 Sep 2021 00:00:00 +0000

kusozako-files (2021.10.07) UNRELEASED; urgency=low

  * css: use "primary-surface-color-class" insted of "toolbar"

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Tue, 21 Sep 2021 00:00:00 +0000

kusozako-files (2021.10.04) UNRELEASED; urgency=low

  * use "secondary-theme-color-class" insted of "bookmark-reeview".

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Tue, 21 Sep 2021 00:00:00 +0000

kusozako-files (2021.09.27) UNRELEASED; urgency=low

  * interim packaging not finished yet.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Tue, 21 Sep 2021 00:00:00 +0000

kusozako-files (2021.09.08) UNRELEASED; urgency=low

  * trash-bin: realod for VISIBLE_AREA signal
  * trash-bin: sort by deletion-unix-time by default.
  * trash-bin: filter model
  * trash-bin: base model modules moved under the filter-model
  * trash-bin: preview status column added to base model
  * trash-bin: reload preview for LOAD_FINISHED signal
  * trash-bin: initial parsing is triggered by DIRECTORY_MODED signal.
  * trash-bin: simplify current dir handling.
  * trash-bin: unify registration massage to register_model_object
  * file-manager: base model modules moved under the filter-model
  * file-manager: unify registration massage to register_model_object

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Wed, 08 Sep 2021 00:00:00 +0000

kusozako-files (2021.09.04) UNRELEASED; urgency=low

  * update for libkusozako3 2021.09.04

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Sat, 04 Sep 2021 00:00:00 +0000

kusozako-files (2021.09.03) UNRELEASED; urgency=low

  * use GLib.utf8_collate_key_for_file to sort filenames instead of REGEX.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Fri, 03 Sep 2021 00:00:00 +0000

kusozako-files (2021.09.02) UNRELEASED; urgency=low

  * bugfix: renaming sequence works fine again.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Thu, 02 Sep 2021 00:00:00 +0000

kusozako-files (2021.09.01) UNRELEASED; urgency=low

  * bugfix: recursive directory copying works fine.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Wed, 01 Sep 2021 00:00:00 +0000

kusozako-files (2021.08.31) UNRELEASED; urgency=low

  * new: sort funcs
  * new: basic keybinds
  * refactor user input sequence

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Tue, 31 Aug 2021 00:00:00 +0000

kusozako-files (2021.08.30) UNRELEASED; urgency=low

  * new: back-to-bookmark button for previewers
  * new: memory side pane size as settings
  * bookmark treeview: avoid activation if tree_row has no valid path data
  * improve: simplify selection sequence.
  * new: file manager message bar
  * new: command line args.
  + new: toggle show hidden button
  + new: select-all, unselect-all, invert-selection

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Thu, 26 Aug 2021 00:00:00 +0000

kusozako-files (2021.08.25) UNRELEASED; urgency=low

  * new: previewers for text, image, video, audio and pdf

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Wed, 25 Aug 2021 00:00:00 +0000

kusozako-files (2021.08.22) UNRELEASED; urgency=low

  * Initial release.

 -- takeda.nemuru <takeda.nemuru@yandex.com>  Sun, 22 Aug 2021 00:00:00 +0000
