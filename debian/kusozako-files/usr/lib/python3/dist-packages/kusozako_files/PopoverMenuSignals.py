
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

POPUP = 0                       # None
REQUEST_CHANGE_THUMBNAIL = 1    # None
COPY_PATH = 2                   # None
COPY_URI = 3                    # None
