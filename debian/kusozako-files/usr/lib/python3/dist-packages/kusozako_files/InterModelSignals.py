
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

# signals                            param:
FILE_MANAGER_NEW = 0                # Gio.file or None for Home Directory
FILE_MANAGER_MOVE_DIRECTORY = 1     # Gio.File for destination directory
TRASH_BIN_NEW = 2                   # None
PREVIEW_SHOW_PREVIEW = 3            # tree_row
