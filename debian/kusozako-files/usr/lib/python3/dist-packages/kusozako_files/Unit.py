
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import math
from libkusozako3.Ux import Unit

TILE_SIZE = Unit(16)
TOOLBAR_HEIGHT = Unit(5)

DEFAULT_SIDE_PANE_WIDTH = Unit(30)

MARGIN = Unit(0.5)

SIZE_04 = TILE_SIZE*0.4
SIZE_06 = TILE_SIZE*0.6
SIZE_10 = TILE_SIZE

ANGLE_NORTH_EAST = math.radians(45)
ANGLE_NORTH_WEST = math.radians(-45)

OFFSET_RATE = 1/math.sqrt(2)

TRIANGLE_HEIGHT = TILE_SIZE*0.6
TRIANGLE_WIDTH = math.sqrt((TRIANGLE_HEIGHT*TRIANGLE_HEIGHT)*2)
