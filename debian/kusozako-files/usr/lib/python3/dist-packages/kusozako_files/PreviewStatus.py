
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

NONE = 0
NEEDLESS = 1
UNLOADED = 2
PENDING = 3
LOADED = 4
FAILED = 5
BROKEN = 6

TARGET_MIME_TYPES = "image", "audio", "video", "application/pdf"


def get_initial_status(mime_type):
    for target in TARGET_MIME_TYPES:
        if mime_type.startswith(target):
            return UNLOADED
    return NEEDLESS
