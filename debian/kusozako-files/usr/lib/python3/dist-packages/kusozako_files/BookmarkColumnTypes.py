
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio

GIO_ICON = 0
TYPE = 1
VISIBLE_NAME = 2
TOOLTIP_TEXT = 3
GIO_FILE = 4
NUMBER_OF_COLUMNS = 5

TYPES = (
    Gio.Icon,       # 0: GIO_ICON
    str,            # 1: TYPE
    str,            # 2: VISIBLE_NAME
    str,            # 3: TOOLTIP_TEXT
    Gio.File        # 4: Gio.File or None
    )
