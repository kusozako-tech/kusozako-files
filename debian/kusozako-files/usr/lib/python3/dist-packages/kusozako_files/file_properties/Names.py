
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib


def get_names(mime_type, file_info):
    file_name = file_info.get_name()
    # test
    lower_file_name = GLib.utf8_strdown(file_name, -1)
    collate_key = GLib.utf8_collate_key_for_filename(lower_file_name, -1)
    return file_name, file_name, collate_key
