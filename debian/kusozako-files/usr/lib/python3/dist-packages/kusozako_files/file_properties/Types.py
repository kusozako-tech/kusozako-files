
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.mime_type import MimeType
from kusozako_files import PreviewStatus


def get_types(file_info, gio_file):
    mime_type = MimeType.from_file_info(gio_file, file_info)
    content_type = file_info.get_content_type()
    is_directory = True if mime_type == "inode/directory" else False
    preview_status = PreviewStatus.get_initial_status(mime_type)
    return mime_type, content_type, is_directory, preview_status
