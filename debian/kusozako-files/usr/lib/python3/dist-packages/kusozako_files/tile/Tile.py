
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerColumnTypes
from libkusozako3.tile.IconTile import DeltaIconTile
from libkusozako3.tile.ThumbnailTile import DeltaThumbnailTile


class FoxtrotTile:

    @classmethod
    def get_default(cls):
        if "_tile" not in dir(cls):
            cls._tile = cls()
        return cls._tile

    def get_simple_tile(self, file_info, gio_file):
        tile_pixbuf = self._icon_tile.build_for_file_info(file_info)
        return None, tile_pixbuf, tile_pixbuf

    def get_preview(self, tree_row):
        gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        return self._thumbnail_tile.build_for_gio_file(gio_file)

    def __init__(self):
        self._icon_tile = DeltaIconTile()
        self._thumbnail_tile = DeltaThumbnailTile()
