
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Ux import Unit


class FoxtrotPixbufPosition:

    def get_positions(self, pixbuf):
        yuki_x = (self._width-pixbuf.get_width())/2
        yuki_y = min(Unit(1), (self._height-pixbuf.get_height())/2)
        return yuki_x, yuki_y

    def __init__(self, size):
        self._width, self._height = size
