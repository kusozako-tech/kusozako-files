
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .OpenFileWith import OPEN_FILE_WITH
from .CopyToClipboard import COPY_TO_CLIPBOARD
from .SelectionOperations import SELECTION_OPERATIONS
from .Rename import RENAME
from .Selection import SELECTION
from .Sort import SORT


MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "switcher",
            "title": _("Open With..."),
            "message": "delta > switch stack to",
            "user-data": "open-file-with",
        },
        {
            "type": "switcher",
            "title": _("Rename"),
            "message": "delta > switch stack to",
            "user-data": "rename",
        },
        *COPY_TO_CLIPBOARD,
        {
            "type": "separator"
        },
        {
            "type": "switcher",
            "title": _("Selection"),
            "message": "delta > switch stack to",
            "user-data": "selection",
        },
        {
            "type": "switcher",
            "title": _("Sort"),
            "message": "delta > switch stack to",
            "user-data": "sort",
        },
        {
            "type": "separator"
        },
        *SELECTION_OPERATIONS
    ]
}

MODEL = [MAIN_PAGE, OPEN_FILE_WITH, RENAME, SELECTION, SORT]
