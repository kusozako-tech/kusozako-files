
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from .header_bar.HeaderBar import DeltaHeaderBar
from .content_area.ContentArea import DeltaContentArea


class DeltaWindow(Gtk.Dialog, DeltaEntity):

    def _delta_call_dialog_response(self, response):
        print("response", response)
        self.response(response)

    def _delta_call_add_to_container(self, widget):
        container = self.get_content_area()
        container.add(widget)

    def __init__(self, parent):
        self._parent = parent
        window = self._enquiry("delta > application window")
        Gtk.Dialog.__init__(self, "", window, Gtk.ResponseType.CANCEL)
        self.set_size_request(Unit(74), Unit(60))
        content_area = self.get_content_area()
        content_area.set_border_width(0)
        self.set_decorated(False)
        self.set_skip_taskbar_hint(True)
        self.set_skip_pager_hint(True)
        self.set_keep_above(True)
        DeltaHeaderBar(self)
        DeltaContentArea(self)
        self.show_all()
