
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaForceReload(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.VIEWERS_FORCE_RELOAD

    def _control(self, param):
        icon_view = self._enquiry("delta > icon view")
        range_ = icon_view.get_visible_range()
        param = FileManagerSignals.MODEL_REQUEST_PREVIEW, range_
        self._raise("delta > model signal", param)
