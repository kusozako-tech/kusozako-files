
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import PopoverMenuSignals


class DeltaCopyPath(DeltaEntity):

    def _action(self):
        gio_file = self._enquiry("delta > gio file")
        path = gio_file.get_path()
        display = Gdk.Display.get_default()
        clipboard = Gtk.Clipboard.get_default(display)
        clipboard.set_text(path, -1)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal == PopoverMenuSignals.COPY_PATH:
            self._action()

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register popover menu object", self)
