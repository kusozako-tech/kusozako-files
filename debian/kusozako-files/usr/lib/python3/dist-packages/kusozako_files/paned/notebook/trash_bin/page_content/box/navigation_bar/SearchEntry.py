
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_files import FileManagerSignals


class DeltaSearchEntry(Gtk.SearchEntry, DeltaEntity):

    def _on_changed(self, search_entry):
        param = FileManagerSignals.FILTER_KEYWORD, search_entry.get_text()
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.SearchEntry.__init__(self, hexpand=True, margin=Unit(1))
        self.connect("changed", self._on_changed)
        self._raise("delta > add to container", self)
