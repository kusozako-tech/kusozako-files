
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.header_label.HeaderLabel import FoxtrotHeaderLabel
from kusozako_files import FileManagerColumnTypes as ColumnTypes
from kusozako_files import PreviewStatus
from kusozako_files.tile.Tile import FoxtrotTile


class FoxtrotPixbufReloader:

    def _get_preview(self, tree_row, pixbuf):
        tree_row[ColumnTypes.PREVIEW_STATUS] = PreviewStatus.PENDING
        original, preview_pixbuf = self._tile.get_preview(tree_row)
        if preview_pixbuf is not None:
            tree_row[ColumnTypes.PREVIEW_STATUS] = PreviewStatus.LOADED
            return preview_pixbuf
        tree_row[ColumnTypes.PREVIEW_STATUS] = PreviewStatus.FAILED
        return pixbuf

    def _get_pixbuf(self, tree_row):
        pixbuf = tree_row[ColumnTypes.SOURCE_PIXBUF]
        preview_status = tree_row[ColumnTypes.PREVIEW_STATUS]
        if preview_status == PreviewStatus.UNLOADED:
            return self._get_preview(tree_row, pixbuf)
        return pixbuf

    def reload_full(self, tree_row):
        gio_file = tree_row[ColumnTypes.GIO_FILE]
        file_info = tree_row[ColumnTypes.FILE_INFO]
        source_pixbuf = self._tile.get_simple_tile(file_info, gio_file)
        source_pixbuf = self._get_preview(tree_row, source_pixbuf)
        tree_row[ColumnTypes.PREVIEW_STATUS] = PreviewStatus.LOADED
        visible_pixbuf = self._header_label.get_visible_pixbuf(source_pixbuf)
        tree_row[ColumnTypes.SOURCE_PIXBUF] = source_pixbuf
        tree_row[ColumnTypes.VISIBLE_PIXBUF] = visible_pixbuf

    def reload(self, tree_row):
        source_pixbuf = self._get_pixbuf(tree_row)
        visible_pixbuf = self._header_label.get_visible_pixbuf(source_pixbuf)
        tree_row[ColumnTypes.SOURCE_PIXBUF] = source_pixbuf
        tree_row[ColumnTypes.VISIBLE_PIXBUF] = visible_pixbuf

    def __init__(self, label_text, label_color):
        self._tile = FoxtrotTile.get_default()
        self._header_label = FoxtrotHeaderLabel.new(label_text, label_color)
