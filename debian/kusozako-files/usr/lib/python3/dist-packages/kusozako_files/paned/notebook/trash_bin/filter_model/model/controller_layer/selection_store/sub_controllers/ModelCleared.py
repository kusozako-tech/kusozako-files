
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaModelCleared(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_CLEARED

    def _control(self, param):
        self._raise("delta > clear")
