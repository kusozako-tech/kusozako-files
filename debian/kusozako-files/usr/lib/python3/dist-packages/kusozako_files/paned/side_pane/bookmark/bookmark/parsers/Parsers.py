
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .UserSpecialDirs import DeltaUserSpecialDirs
from .Separator import DeltaSeparator
from .Gtk3 import DeltaGtk3


class EchoParsers:

    def __init__(self, parent):
        DeltaUserSpecialDirs(parent)
        DeltaSeparator(parent)
        DeltaGtk3(parent)
