
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import cairo
from gi.repository import Gdk
from libkusozako3.Ux import Unit
from .CairoContext import FoxtrotCairoContext

CONTENT_TYPE = cairo.CONTENT_COLOR_ALPHA
SURFACE = cairo.ImageSurface(cairo.Format.ARGB32, Unit(16), Unit(16))


class FoxtrotSurface:

    @classmethod
    def new_tile(cls, rgba=None):
        return cls((Unit(16), Unit(16)), rgba)

    def paint_pixbuf(self, pixbuf):
        self._context.paint_pixbuf(pixbuf)
        return Gdk.pixbuf_get_from_surface(self._surface, 0, 0, *self._size)

    def __init__(self, size, rgba=None):
        self._size = size
        self._surface = SURFACE.create_similar(CONTENT_TYPE, *size)
        self._context = FoxtrotCairoContext(self._surface, size, rgba)
