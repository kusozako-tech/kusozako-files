
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CopyTo import DeltaCopyTo
from .Restore import DeltaRestore
from .Delete import DeltaDelete


class EchoOperations:

    def __init__(self, parent):
        DeltaCopyTo(parent)
        DeltaRestore(parent)
        DeltaDelete(parent)
