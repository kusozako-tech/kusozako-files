
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import BookmarkColumnTypes
from .tree_view.TreeView import DeltaTreeView
from .parsers.Parsers import EchoParsers


class DeltaBokmark(Gtk.ListStore, DeltaEntity):

    def _delta_info_model(self):
        return self

    def _delta_call_append_row_data(self, row_data):
        self.append(row_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *BookmarkColumnTypes.TYPES)
        EchoParsers(self)
        DeltaTreeView(self)
