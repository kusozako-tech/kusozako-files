
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_system_monitor import SystemMonitorSignals


class BravoExtraFuncs:

    def _on_clicked(self, tree_view_column, column_id):
        param = SystemMonitorSignals.PIDS_VIWER_SORT_COLUMN_CHANGED, column_id
        self._raise("delta > system monitor signal", param)

    def _set_stripe(self, column, renderer, model, tree_iter, data=None):
        tree_path = model.get_path(tree_iter)
        color = "White" if tree_path[0] % 2 == 0 else "LightGrey"
        renderer.set_property("cell-background", color)

    def _set_sort_column(self, column_id):
        self.set_sort_column_id(column_id)
        self.connect("clicked", self._on_clicked, column_id)
