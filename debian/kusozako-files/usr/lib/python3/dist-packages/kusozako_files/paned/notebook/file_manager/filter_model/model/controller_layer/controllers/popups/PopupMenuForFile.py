
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .popover_models.FilePopoverMenuModel import MODEL
from .PopupMenuForItem import AlfaPopupMenuForItem


class DeltaPopupMenuForFile(AlfaPopupMenuForItem):

    MODEL = MODEL
