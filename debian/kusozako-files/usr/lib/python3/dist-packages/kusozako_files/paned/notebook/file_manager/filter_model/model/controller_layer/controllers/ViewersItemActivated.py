
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerColumnTypes
from kusozako_files import FileManagerSignals
from kusozako_files import InterModelSignals


class DeltaViewersItemActivated(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.VIEWERS_ITEM_ACTIVATED

    def _force_select(self, tree_row):
        param = FileManagerSignals.SELECTION_FORCE_SELECT, tree_row
        self._raise("delta > model signal", param)

    def _show_preview(self, tree_row):
        param = InterModelSignals.PREVIEW_SHOW_PREVIEW, tree_row
        self._raise("delta > inter model signal", param)

    def _move_directroy_to(self, tree_row):
        gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        param = FileManagerSignals.MODEL_MOVE_DIRECTORY, gio_file
        self._raise("delta > model signal", param)

    def _activate_tree_row(self, tree_row):
        mime_type = tree_row[FileManagerColumnTypes.MIME_TYPE]
        if mime_type != "inode/directory":
            self._show_preview(tree_row)
        else:
            self._move_directroy_to(tree_row)

    def _control(self, tree_row):
        self._force_select(tree_row)
        self._activate_tree_row(tree_row)
