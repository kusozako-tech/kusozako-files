
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from kusozako_files import FileManagerSignals
from .filter_model.FilterModel import DeltaFilterModel
from .page_content.PageContent import DeltaPageContent


class DeltaTrashBin(DeltaEntity):

    def _delta_call_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_info_filter_model(self):
        return self._filter_model.model

    def _delta_call_register_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def _delta_call_close_tab(self):
        self._raise("delta > remove page", self._page_content)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        self._filter_model = DeltaFilterModel(self)
        DeltaPageContent(self)
        data = FileManagerSignals.MODEL_MOVE_DIRECTORY, None
        self._transmitter.transmit(data)
