
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.file_chooser.FileChooser import DeltaFileChooser
from kusozako_files import FileManagerSignals
from .FileOperation import AlfaFileOperation
from .FileManeuver import FoxtrotFileManeuver

FILE_CHOOSER_MODEL = {
    "type": "select-directory",
    "id": "com.kusozako-tech.kusozako-files.trash-restore",
    "title": _("Select Directory"),
    "directory": GLib.get_home_dir(),
    "read-write-only": True
    }


class DeltaCopyTo(AlfaFileOperation):

    SIGNAL = FileManagerSignals.SELECTION_OPERATION_COPY_TO
    QUEUE_SIGNAL = FileManagerSignals.SELECTION_CAN_READ_CHANGED

    def _delta_call_gio_file_queued(self, gio_file):
        file_maneuver = FoxtrotFileManeuver.get_default()
        file_maneuver.copy_recursive_async(
            self._target_directory,
            gio_file.get_path()
            )

    def _get_operation_confirmed_by_user(self):
        gio_file = self._async_queue.selection[0]
        parent_gio_file = gio_file.get_parent()
        directory = DeltaFileChooser.run_for_model(self, FILE_CHOOSER_MODEL)
        self._target_directory = directory
        return directory is not None
