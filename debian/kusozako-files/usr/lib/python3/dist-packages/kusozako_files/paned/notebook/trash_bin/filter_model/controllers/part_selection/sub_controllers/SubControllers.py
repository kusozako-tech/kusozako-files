
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SelectAfter import DeltaSelectAfter
from .UnselectAfter import DeltaUnselectAfter
from .SelectBefore import DeltaSelectBefore
from .UnselectBefore import DeltaUnselectBefore


class EchoSubControllers:

    def __init__(self, parent):
        DeltaSelectAfter(parent)
        DeltaUnselectAfter(parent)
        DeltaSelectBefore(parent)
        DeltaUnselectBefore(parent)
