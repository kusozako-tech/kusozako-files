
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals
from .PopupMenuForNonItem import DeltaPopupMenuForNonItem


class DeltaViewersPopupNonItemPopup(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.VIEWERS_POPUP_NON_ITEM_POPUP

    def _control(self, user_data):
        widget, x, y = user_data
        self._popup_menu.popup(widget, x, y)

    def _on_initialize(self):
        self._popup_menu = DeltaPopupMenuForNonItem(self)
