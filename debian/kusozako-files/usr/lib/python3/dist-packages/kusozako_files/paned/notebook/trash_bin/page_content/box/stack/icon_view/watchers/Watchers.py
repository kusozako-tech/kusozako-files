
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .VAdjustment import DeltaVAdjustment
from .QueryTooltip import DeltaQueryTooltip
from .button_release_event.ButtonReleaseEvent import DeltaButtonReleaseEvent
from .key_press_event.KeyPressEvent import DeltaKeyPressEvent


class EchoWatchers:

    def __init__(self, parent):
        DeltaVAdjustment(parent)
        DeltaQueryTooltip(parent)
        DeltaButtonReleaseEvent(parent)
        DeltaKeyPressEvent(parent)
