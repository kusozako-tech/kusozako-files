
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals
from .icon_view.IconView import DeltaIconView
from .tree_view.TreeView import DeltaTreeView


class DeltaStack(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _on_realize(self, stack):
        data = FileManagerSignals.VIEWERS_SWITCH_TO, "icon-view"
        self._raise("delta > model signal", data)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileManagerSignals.VIEWERS_SWITCH_TO:
            self.set_visible_child_name(param)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(self)
        self.connect("realize", self._on_realize)
        self.set_transition_type(Gtk.StackTransitionType.OVER_RIGHT_LEFT)
        DeltaIconView(self)
        DeltaTreeView(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
