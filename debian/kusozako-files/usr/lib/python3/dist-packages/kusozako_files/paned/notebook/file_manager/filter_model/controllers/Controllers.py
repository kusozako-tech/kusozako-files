
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .request_preview.RequestPreview import DeltaRequestPreview
from .SelectAll import DeltaSelectAll
from .UnselectAll import DeltaUnselectAll
from .InvertSelection import DeltaInvertSelection
from .part_selection.PartSelection import DeltaPartSelection


class EchoControllers:

    def __init__(self, parent):
        DeltaPartSelection(parent)
        DeltaRequestPreview(parent)
        DeltaSelectAll(parent)
        DeltaUnselectAll(parent)
        DeltaInvertSelection(parent)
