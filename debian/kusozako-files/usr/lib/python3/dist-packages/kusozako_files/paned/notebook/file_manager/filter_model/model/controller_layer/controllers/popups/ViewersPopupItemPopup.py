
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals
from kusozako_files import FileManagerColumnTypes
from .PopupMenuForFile import DeltaPopupMenuForFile
from .PopupMenuForDirectory import DeltaPopupMenuForDirectory


class DeltaViewersPopupItemPopup(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.VIEWERS_POPUP_ITEM_POPUP

    def _control(self, user_data):
        tree_row, widget, x, y = user_data
        mime_type = tree_row[FileManagerColumnTypes.MIME_TYPE]
        param = FileManagerSignals.SELECTION_FORCE_SELECT, tree_row
        self._raise("delta > model signal", param)
        if mime_type == "inode/directory":
            self._popup_menu_for_directory.popup(tree_row, widget, x, y)
        else:
            self._popup_menu_for_file.popup(tree_row, widget, x, y)

    def _on_initialize(self):
        self._popup_menu_for_directory = DeltaPopupMenuForDirectory(self)
        self._popup_menu_for_file = DeltaPopupMenuForFile(self)
