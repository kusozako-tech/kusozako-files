
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from .Selection import AlfaSelection


class DeltaCanWriteSelection(AlfaSelection):

    SIGNAL = FileManagerSignals.SELECTION_CAN_WRITE_CHANGED
    ATTRIBUTE = "access::can-write"
