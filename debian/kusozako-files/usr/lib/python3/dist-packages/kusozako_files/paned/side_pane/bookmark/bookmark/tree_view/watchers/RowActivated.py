
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import InterModelSignals
from kusozako_files import BookmarkColumnTypes


class DeltaRowActivated(DeltaEntity):

    def _on_row_activated(self, tree_view, tree_path, column):
        model = tree_view.get_model()
        gio_file = model[tree_path][BookmarkColumnTypes.GIO_FILE]
        if gio_file is None or not gio_file.query_exists():
            return
        param = InterModelSignals.FILE_MANAGER_MOVE_DIRECTORY, gio_file
        self._raise("delta > inter model signal", param)

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.connect("row-activated", self._on_row_activated)
