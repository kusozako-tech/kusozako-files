
# (c) copyright 2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from .ExtraFuncs import BravoExtraFuncs


class DeltaLastModified(Gtk.TreeViewColumn, DeltaEntity, BravoExtraFuncs):

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(xalign=1)
        Gtk.TreeViewColumn.__init__(
            self,
            title=_("Last Modified"),
            cell_renderer=renderer,
            text=FileManagerColumnTypes.LAST_MODIFIED_READABLE
            )
        self._set_sort_column(FileManagerColumnTypes.LAST_MODIFIED_UNIX)
        self.set_resizable(True)
        self.set_expand(False)
        self._set_stripe2(renderer)
        self._raise("delta > append tree view column", self)
