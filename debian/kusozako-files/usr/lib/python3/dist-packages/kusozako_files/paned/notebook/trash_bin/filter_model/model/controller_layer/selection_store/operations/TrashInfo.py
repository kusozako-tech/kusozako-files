
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib

NAMES = [GLib.get_user_data_dir(), "Trash", "info"]
INFO_DIRECTORY = GLib.build_filenamev(NAMES)


def _get_trash_info(trash_file):
    names = [INFO_DIRECTORY, trash_file.get_basename()]
    info_path = GLib.build_filenamev(names)+".trashinfo"
    if not GLib.file_test(info_path, GLib.FileTest.EXISTS):
        return info_path, None
    info_file = Gio.File.new_for_path(info_path)
    return info_path, info_file


def get_info_file(trash_file):
    _, info_file = _get_trash_info(trash_file)
    return info_file


def get_destination_file(trash_file):
    info_path, info_file = _get_trash_info(trash_file)
    if info_file is None:
        return None, None
    key_file = GLib.KeyFile.new()
    key_file.load_from_file(info_path, GLib.KeyFileFlags.NONE)
    original_destination = key_file.get_string("Trash Info", "Path")
    destination_file = Gio.File.new_for_uri("file://"+original_destination)
    return destination_file, info_file
