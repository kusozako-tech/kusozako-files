
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes

FORMAT = "\n\nSymbolic Link Target:\n\t{}"


class DeltaQueryTooltip(DeltaEntity):

    def _get_tooltip_text(self, tree_row):
        base_text = tree_row[FileManagerColumnTypes.TOOLTIP_TEXT]
        file_info = tree_row[FileManagerColumnTypes.FILE_INFO]
        original_path = tree_row[FileManagerColumnTypes.ORIGINAL_PATH]
        base_text += "\n\nOriginal Path:\n\t{}".format(original_path)
        deletion_time = tree_row[FileManagerColumnTypes.DELETION_TIME_READABLE]
        base_text += "\n\nDeletion Time:\n\t{}".format(deletion_time)
        if file_info.get_is_symlink():
            symlink_target = file_info.get_symlink_target()
            gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
            target_file = gio_file.resolve_relative_path(symlink_target)
            base_text += FORMAT.format(target_file.get_path())
        return base_text

    def _get_tree_row(self, icon_view, x, y):
        model = icon_view.get_model()
        bin_x, bin_y = icon_view.convert_widget_to_bin_window_coords(x, y)
        response = icon_view.get_item_at_pos(bin_x, bin_y)
        if response is None:
            return None
        tree_path, _ = response
        return model[tree_path]

    def _on_query_tooltip(self, icon_view, x, y, keyboard_mode, tooltip):
        tree_row = self._get_tree_row(icon_view, x, y)
        if tree_row is None:
            return False
        tooltip.set_icon(tree_row[FileManagerColumnTypes.ORIGINAL_PIXBUF])
        tooltip.set_text(self._get_tooltip_text(tree_row))
        return True

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        icon_view.props.has_tooltip = True
        icon_view.connect("query-tooltip", self._on_query_tooltip)
