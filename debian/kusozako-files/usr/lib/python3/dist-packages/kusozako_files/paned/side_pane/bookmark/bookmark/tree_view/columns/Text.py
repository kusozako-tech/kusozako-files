
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import Pango
from libkusozako3.Entity import DeltaEntity
from kusozako_files import BookmarkColumnTypes


class DeltaText(Gtk.TreeViewColumn, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererText(ellipsize=Pango.EllipsizeMode.END)
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer,
            text=BookmarkColumnTypes.VISIBLE_NAME
            )
        self.set_expand(True)
        self._raise("delta > append tree view column", self)
