
# (c) copyright 2021-2022, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from .RowConstructor import BravoRowConstructor

ICON_NAMES = {
    GLib.UserDirectory.DIRECTORY_DESKTOP: "user-desktop-symbolic",
    GLib.UserDirectory.DIRECTORY_DOCUMENTS: "folder-documents-symbolic",
    GLib.UserDirectory.DIRECTORY_DOWNLOAD: "folder-download-symbolic",
    GLib.UserDirectory.DIRECTORY_MUSIC: "folder-music-symbolic",
    GLib.UserDirectory.DIRECTORY_PICTURES: "folder-pictures-symbolic",
    GLib.UserDirectory.DIRECTORY_PUBLIC_SHARE: "folder-publicshare-symbolic",
    GLib.UserDirectory.DIRECTORY_TEMPLATES: "folder-templates-symbolic",
    GLib.UserDirectory.DIRECTORY_VIDEOS: "folder-videos-symbolic",
}


class DeltaUserSpecialDirs(DeltaEntity, BravoRowConstructor):

    def __init__(self, parent):
        self._parent = parent
        home_directory = GLib.get_home_dir()
        gio_file = Gio.File.new_for_path(home_directory)
        self._construct("xdg-user-special-dir", gio_file, "user-home-symbolic")
        for index in range(0, GLib.UserDirectory.N_DIRECTORIES):
            path = GLib.get_user_special_dir(index)
            if home_directory == path:
                continue
            icon_name = ICON_NAMES[index]
            gio_file = Gio.File.new_for_path(path)
            self._construct("xdg-user-special-dir", gio_file, icon_name)
