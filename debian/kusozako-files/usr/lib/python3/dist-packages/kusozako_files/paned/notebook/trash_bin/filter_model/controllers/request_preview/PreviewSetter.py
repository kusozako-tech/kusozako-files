
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from kusozako_files import FileManagerColumnTypes as ColumnTypes
from kusozako_files import PreviewStatus
from kusozako_files.tile.Tile import FoxtrotTile
from kusozako_files.header_label.HeaderLabel import FoxtrotHeaderLabel

HEADER_PARAM = _("SELECTED"), (255/255, 165/255, 0/255, 0.9)


class FoxtrotPreviewSetter:

    def _try_get_is_selected(self, tree_row):
        try:
            is_selected = tree_row[ColumnTypes.SELECTED]
            return is_selected
        except TypeError:
            # directory has been moved:
            return None

    def _get_visible_pixbuf(self, tree_row, pixbuf):
        is_selected = self._try_get_is_selected(tree_row)
        if is_selected is None:
            return None
        if not is_selected:
            return pixbuf.copy()
        return self._header_label.get_visible_pixbuf(pixbuf)

    def _set_preview(self, tree_row):
        original, pixbuf = self._tile.get_preview(tree_row)
        if pixbuf is not None:
            tree_row[ColumnTypes.ORIGINAL_PIXBUF] = original
            visible_pixbuf = self._get_visible_pixbuf(tree_row, pixbuf)
            tree_row[ColumnTypes.SOURCE_PIXBUF] = pixbuf
            tree_row[ColumnTypes.VISIBLE_PIXBUF] = visible_pixbuf
            tree_row[ColumnTypes.PREVIEW_STATUS] = PreviewStatus.LOADED
        else:
            tree_row[ColumnTypes.PREVIEW_STATUS] = PreviewStatus.FAILED

    def _timeout(self, tree_row_queue):
        if tree_row_queue.empty():
            return GLib.SOURCE_REMOVE
        GLib.idle_add(self._set_preview, tree_row_queue.get())
        return GLib.SOURCE_CONTINUE

    def set_preview_async(self, tree_row_queue):
        GLib.timeout_add(10, self._timeout, tree_row_queue)

    def __init__(self):
        self._tile = FoxtrotTile.get_default()
        self._header_label = FoxtrotHeaderLabel.new(*HEADER_PARAM)
