
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals


class DeltaDeleteButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        param = FileManagerSignals.SELECTION_OPERATION_DELETE_TRASH, None
        self._raise("delta > model signal", param)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == FileManagerSignals.SELECTION_CAN_READ_CHANGED:
            self.set_sensitive(param)

    def __init__(self, parent):
        self._parent = parent
        icon_params = "edit-delete-symbolic", Gtk.IconSize.SMALL_TOOLBAR
        image = Gtk.Image.new_from_icon_name(*icon_params)
        Gtk.Button.__init__(self, relief=Gtk.ReliefStyle.NONE, image=image)
        self.props.tooltip_text = _("Delete")
        self.connect("clicked", self._on_clicked)
        self.set_sensitive(False)
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
