
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

ICON_SIZE = Gtk.IconSize.SMALL_TOOLBAR


class DeltaToggleShowHiddenButton(Gtk.Button, DeltaEntity):

    def _on_clicked(self, button):
        data = FileManagerSignals.FILTER_TOGGLE_SHOW_HIDDEN, self._show_hidden
        self._raise("delta > model signal", data)

    def _toggle_show_hidden(self, show_hidden):
        self._show_hidden = not show_hidden
        if show_hidden:
            self._image.set_from_icon_name("view-reveal-symbolic", ICON_SIZE)
        else:
            self._image.set_from_icon_name("view-conceal-symbolic", ICON_SIZE)

    def receive_transmission(self, user_data):
        signal, show_hidden = user_data
        if signal == FileManagerSignals.FILTER_TOGGLE_SHOW_HIDDEN:
            self._toggle_show_hidden(show_hidden)

    def __init__(self, parent):
        self._parent = parent
        self._show_hidden = True
        icon_params = "view-conceal-symbolic", ICON_SIZE
        self._image = Gtk.Image.new_from_icon_name(*icon_params)
        Gtk.Button.__init__(
            self,
            relief=Gtk.ReliefStyle.NONE,
            image=self._image
            )
        self.props.tooltip_text = _("Toggle Show Hoidden")
        self.connect("clicked", self._on_clicked)
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
