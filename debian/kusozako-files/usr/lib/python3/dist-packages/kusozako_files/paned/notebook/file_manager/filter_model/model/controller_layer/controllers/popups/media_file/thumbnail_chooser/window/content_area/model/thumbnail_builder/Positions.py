
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaPositions(DeltaEntity):

    def _timeout(self):
        is_active = self._enquiry("delta > is active")
        if not is_active:
            return GLib.SOURCE_REMOVE
        position = self._positions.pop(0)
        self._raise("delta > position", position)
        return len(self._positions) > 0

    def __init__(self, parent):
        self._parent = parent
        self._positions = []
        current = 0
        while 10000 > current:
            self._positions.append(current)
            current += GLib.random_int_range(50, 250)
        self._positions.append(10000)
        GLib.timeout_add(100, self._timeout)
