
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerColumnTypes
from .Selected import DeltaSelected
from .Icon import DeltaIcon
from .FileName import DeltaFileName
from .PropertyColumn import DeltaPropertyColumn


class EchoColumns:

    def __init__(self, parent):
        DeltaSelected(parent)
        DeltaIcon(parent)
        DeltaFileName(parent)
        DeltaPropertyColumn.new(
            parent,
            _("Deletion Time"),
            FileManagerColumnTypes.DELETION_TIME_READABLE,
            FileManagerColumnTypes.DELETION_TIME_UNIX
            )
