
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController


class DeltaEntryTextChanged(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_LOADED

    def _try_move_directory(self, gio_file):
        path = gio_file.get_path()
        is_directory = GLib.file_test(path, GLib.FileTest.IS_DIR)
        if is_directory:
            data = FileManagerSignals.MODEL_MOVE_DIRECTORY, gio_file
            self._raise("delta > model signal", data)
            data = FileManagerSignals.FILTER_KEYWORD, ""
            self._raise("delta > model signal", data)
        return is_directory

    def _try_set_filter(self, gio_file):
        parent = gio_file.get_parent()
        if not parent.equal(self._directory):
            return
        filter_ = gio_file.get_basename()
        data = FileManagerSignals.FILTER_KEYWORD, filter_
        self._raise("delta > model signal", data)

    def _on_changed(self, entry):
        if self._enquiry("delta > locked"):
            return
        gio_file = Gio.File.new_for_path(entry.get_text())
        if self._try_move_directory(gio_file):
            return
        self._try_set_filter(gio_file)

    def _control(self, gio_file):
        self._directory = gio_file

    def _on_initialize(self):
        self._directory = None
        entry = self._enquiry("delta > entry")
        entry.connect("changed", self._on_changed)
