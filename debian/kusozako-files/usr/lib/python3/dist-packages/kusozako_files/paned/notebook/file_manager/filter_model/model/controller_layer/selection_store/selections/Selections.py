
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CanReadSelection import DeltaCanReadSelection
from .CanWriteSelection import DeltaCanWriteSelection
from .CanTrashSelection import DeltaCanTrashSelection


class EchoSelections:

    def append(self, user_data):
        self._can_write_selection.append(user_data)
        self._can_read_selection.append(user_data)
        self._can_trash_selection.append(user_data)

    def clear(self):
        self._can_write_selection.clear()
        self._can_read_selection.clear()
        self._can_trash_selection.clear()

    def remove(self, path):
        self._can_write_selection.remove(path)
        self._can_read_selection.remove(path)
        self._can_trash_selection.remove(path)

    def __init__(self, parent):
        self._can_write_selection = DeltaCanWriteSelection(parent)
        self._can_read_selection = DeltaCanReadSelection(parent)
        self._can_trash_selection = DeltaCanTrashSelection(parent)
