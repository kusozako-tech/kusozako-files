
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes


class AlfaSelection(DeltaEntity):

    SIGNAL = "define raising sigal here."
    ATTRIBUTE = "define attribute to check here."

    def _raise_signal(self):
        param = self.SIGNAL, self._store
        self._raise("delta > model signal", param)

    def append(self, user_data):
        tree_row, path = user_data
        file_info = tree_row[FileManagerColumnTypes.FILE_INFO]
        if not file_info.get_attribute_boolean(self.ATTRIBUTE):
            return
        self._store[path] = tree_row
        self._raise_signal()

    def remove(self, path):
        if path not in self._store:
            return
        del self._store[path]
        self._raise_signal()

    def clear(self):
        if not self._store:
            return
        self._store.clear()
        self._raise_signal()

    def __init__(self, parent):
        self._parent = parent
        self._store = {}
