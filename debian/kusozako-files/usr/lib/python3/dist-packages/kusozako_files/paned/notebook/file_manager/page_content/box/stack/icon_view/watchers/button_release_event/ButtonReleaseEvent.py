
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals
from .EventDispatch import DeltaEventDispatch


class DeltaButtonReleaseEvent(DeltaEntity):

    def _delta_call_activate(self, tree_row):
        param = FileManagerSignals.VIEWERS_ITEM_ACTIVATED, tree_row
        self._raise("delta > model signal", param)

    def _delta_call_toggle_selection(self, tree_row):
        param = FileManagerSignals.SELECTION_TOGGLE, tree_row
        self._raise("delta > model signal", param)

    def _get_position(self, event):
        icon_view = self._enquiry("delta > icon view")
        v_adjustment = icon_view.get_vadjustment()
        return icon_view, event.x, event.y-v_adjustment.get_value()

    def _delta_call_item_popup(self, user_data):
        event, tree_row = user_data
        signal_param = tree_row, *self._get_position(event)
        param = FileManagerSignals.VIEWERS_POPUP_ITEM_POPUP, signal_param
        self._raise("delta > model signal", param)

    def _delta_call_non_item_popup(self, event):
        signal_param = self._get_position(event)
        param = FileManagerSignals.VIEWERS_POPUP_NON_ITEM_POPUP, signal_param
        self._raise("delta > model signal", param)

    def __init__(self, parent):
        self._parent = parent
        DeltaEventDispatch(self)
