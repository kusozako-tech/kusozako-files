
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals


SELECTION_OPERATIONS = (
    {
        "type": "simple-action",
        "title": _("Copy To ..."),
        "message": "delta > model signal",
        "user-data": (FileManagerSignals.SELECTION_OPERATION_COPY_TO, None),
        "close-on-clicked": True
    },
    {
        "type": "simple-action",
        "title": _("Send To ..."),
        "message": "delta > model signal",
        "user-data": (FileManagerSignals.SELECTION_OPERATION_MOVE_TO, None),
        "close-on-clicked": True
    },
    {
        "type": "simple-action",
        "title": _("Remove"),
        "message": "delta > model signal",
        "user-data": (FileManagerSignals.SELECTION_OPERATION_TRASH, None),
        "close-on-clicked": True
    }
)
