
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.message_dialog.MessageDialog import DeltaMessageDialog
from kusozako_files import FileManagerSignals
from .FileOperation import AlfaFileOperation
from . import TrashInfo

MESSAGE = _("""Delete Selected Files ?

SELECTED FILES SHOULD BE DELETED COMPLETELY.
YOU CAN'T UNDO THIS OPERATION.
""")

DIALOG_MODEL = {
    "default-response": 0,
    "icon-name": "dialog-question-symbolic",
    "message": MESSAGE,
    "buttons": (_("Cancel"), _("Delete"))
    }


class DeltaDelete(AlfaFileOperation):

    SIGNAL = FileManagerSignals.SELECTION_OPERATION_DELETE_TRASH
    QUEUE_SIGNAL = FileManagerSignals.SELECTION_CAN_READ_CHANGED

    def _delete_directory(self, gio_file):
        file_enumerator = gio_file.enumerate_children(
            "*",
            Gio.FileQueryInfoFlags.NOFOLLOW_SYMLINKS
            )
        for file_info in file_enumerator:
            gio_file = file_enumerator.get_child(file_info)
            self._delete(gio_file)
        file_enumerator.close()

    def _delete(self, gio_file):
        file_info = gio_file.query_info("*", 0)
        if file_info.get_content_type() == "inode/directory":
            self._delete_directory(gio_file)
        gio_file.delete(None)

    def _delta_call_gio_file_queued(self, gio_file):
        info_file = TrashInfo.get_info_file(gio_file)
        if info_file is None:
            print("Trash Info Not Found.")
            return
        GLib.idle_add(self._delete, gio_file)
        GLib.idle_add(self._delete, info_file)

    def _get_operation_confirmed_by_user(self):
        response = DeltaMessageDialog.run_for_model(self, DIALOG_MODEL)
        return response != 0
