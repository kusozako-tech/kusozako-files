
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .SelectionOperations import SELECTION_OPERATIONS
from .SelectionSimple import SELECTION_SIMPLE
from .Sort import SORT


MAIN_PAGE_FOR_DIRECTORY = {
    "page-name": "main",
    "items": [
        {
            "type": "simple-action",
            "title": _("Open in New Tab"),
            "message": "delta > open in new tab",
            "user-data": None,
            "close-on-clicked": True
        },
        {
            "type": "separator"
        },
        {
            "type": "switcher",
            "title": _("Selection"),
            "message": "delta > switch stack to",
            "user-data": "selection-simple",
        },
        {
            "type": "switcher",
            "title": _("Sort"),
            "message": "delta > switch stack to",
            "user-data": "sort",
        },
        {
            "type": "separator"
        },
        *SELECTION_OPERATIONS
    ]
}

MODEL = [MAIN_PAGE_FOR_DIRECTORY, SELECTION_SIMPLE, SORT]
