
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaCurrentDirectoryRemoved(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.CURRENT_DIRECTORY_REMOVED

    def _control(self, param):
        message_label = _("Current Directory Removed")
        self._raise("delta > label changed", message_label)
