
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .LastEventType import FoxtrotLastEventType


class DeltaEventDispatch(DeltaEntity):

    def _non_item_event(self, event):
        if event.button == 3:
            self._raise("delta > non item popup", event)

    def _on_single_click(self, event, tree_row):
        if event.button == 1:
            self._raise("delta > toggle selection", tree_row)
        elif event.button == 3:
            self._raise("delta > item popup", (event, tree_row))

    def _event_dispatch(self, event, tree_row):
        if self._last_event_type.is_selection_event():
            self._on_single_click(event, tree_row)
        elif self._last_event_type.is_activation_event():
            self._raise("delta > activate", tree_row)

    def _on_button_release(self, widget, event):
        tree_path = widget.get_path_at_pos(event.x, event.y)
        if tree_path is None:
            self._non_item_event(event)
        else:
            model = widget.get_model()
            tree_row = model[tree_path]
            self._event_dispatch(event, tree_row)

    def __init__(self, parent):
        self._parent = parent
        icon_view = self._enquiry("delta > icon view")
        icon_view.connect("button-release-event", self._on_button_release)
        self._last_event_type = FoxtrotLastEventType(icon_view)
