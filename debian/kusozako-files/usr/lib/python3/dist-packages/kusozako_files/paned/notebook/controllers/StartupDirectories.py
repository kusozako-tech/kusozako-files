
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity


class DeltaStartupDirectories(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        files = self._enquiry("delta > command line files")
        if not files:
            gio_file = Gio.File.new_for_path(GLib.get_current_dir())
            self._raise("delta > add file manager", gio_file)
        for gio_file in files:
            self._raise("delta > add file manager", gio_file)
