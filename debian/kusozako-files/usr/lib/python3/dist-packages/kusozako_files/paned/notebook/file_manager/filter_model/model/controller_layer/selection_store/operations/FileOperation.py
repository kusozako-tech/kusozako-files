
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from .AsyncQueue import DeltaAsyncQueue


class AlfaFileOperation(AlfaFileManagerController):

    QUEUE_SIGNAL = "define queue signal to store here."

    def _control(self, param=None):
        if not self._async_queue.has_queue:
            return
        if self._get_operation_confirmed_by_user():
            self._async_queue.start_queuing()

    def _on_initialize(self):
        self._async_queue = DeltaAsyncQueue.new_for_signal(
            self,
            self.QUEUE_SIGNAL
            )
