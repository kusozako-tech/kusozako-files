
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_files import FileManagerSignals

ORDER_CHANGED = FileManagerSignals.SORT_ORDER_CHANGED


SORT_ORDER = (
    {
        "type": "check-action",
        "title": _("A to Z"),
        "message": "delta > model signal",
        "user-data": (ORDER_CHANGED, Gtk.SortType.ASCENDING),
        "close-on-clicked": True,
        "query": "delta > sort order",
        "query-data": None,
        "check-value": Gtk.SortType.ASCENDING
    },
    {
        "type": "check-action",
        "title": _("Z to A"),
        "message": "delta > model signal",
        "user-data": (ORDER_CHANGED, Gtk.SortType.DESCENDING),
        "close-on-clicked": True,
        "query": "delta > sort order",
        "query-data": None,
        "check-value": Gtk.SortType.DESCENDING
    },
)
