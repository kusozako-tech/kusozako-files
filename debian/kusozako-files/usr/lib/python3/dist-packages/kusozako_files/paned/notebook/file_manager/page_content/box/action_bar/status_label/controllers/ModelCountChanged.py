
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaModelCountChanged(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_COUNT_CHANGED

    def _control(self, param):
        directories, files = param
        text = "{} directories and {} files, {} total".format(
            directories,
            files,
            directories+files
            )
        self._raise("delta > count text changed", text)
