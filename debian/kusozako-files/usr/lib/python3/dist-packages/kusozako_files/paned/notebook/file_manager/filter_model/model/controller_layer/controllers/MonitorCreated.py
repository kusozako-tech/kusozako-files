
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from .MonitorRowInserted import AlfaMonitorRowInserted


class DeltaMonitorCreated(AlfaMonitorRowInserted):

    SIGNAL = FileManagerSignals.MONITOR_CREATED
    HEADER_PROPERTY = "CREATED", (62/255, 220/255, 46/255, 0.9)
