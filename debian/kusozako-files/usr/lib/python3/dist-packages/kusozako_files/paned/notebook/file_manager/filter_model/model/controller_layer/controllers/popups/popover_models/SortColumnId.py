
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from kusozako_files import FileManagerColumnTypes as Types

COLUMN_CHANGED = FileManagerSignals.SORT_COLUMN_CHANGED


SORT_COLUMN_ID = (
    {
        "type": "check-action",
        "title": _("By Selected"),
        "message": "delta > model signal",
        "user-data": (COLUMN_CHANGED, Types.SELECTED),
        "close-on-clicked": True,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": Types.SELECTED
    },
    {
        "type": "check-action",
        "title": _("By File Name"),
        "message": "delta > model signal",
        "user-data": (COLUMN_CHANGED, Types.FILE_NAME_COLLATE_KEY),
        "close-on-clicked": True,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": Types.FILE_NAME_COLLATE_KEY
    },
    {
        "type": "check-action",
        "title": _("By File Size"),
        "message": "delta > model signal",
        "user-data": (COLUMN_CHANGED, Types.FILE_SIZE),
        "close-on-clicked": True,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": Types.FILE_SIZE
    },
    {
        "type": "check-action",
        "title": _("By Content Type"),
        "message": "delta > model signal",
        "user-data": (COLUMN_CHANGED, Types.CONTENT_TYPE),
        "close-on-clicked": True,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": Types.CONTENT_TYPE
    },
    {
        "type": "check-action",
        "title": _("By Last Modified"),
        "message": "delta > model signal",
        "user-data": (COLUMN_CHANGED, Types.LAST_MODIFIED_UNIX),
        "close-on-clicked": True,
        "query": "delta > sort column id",
        "query-data": None,
        "check-value": Types.LAST_MODIFIED_UNIX
    },
)
