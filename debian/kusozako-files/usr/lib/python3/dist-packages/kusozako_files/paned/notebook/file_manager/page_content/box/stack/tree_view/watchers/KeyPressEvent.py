
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .UserInputEvent import DeltaUserInputEvent


class DeltaKeyPressEvent(DeltaUserInputEvent):

    EVENT = "key-press-event"

    def _event_dispatch(self, event, tree_row):
        # if "enter"
        if event.keyval == 65293:
            self._try_activate(tree_row)
        # elif "space"
        elif event.keyval == 32:
            self._toggle_selection(tree_row)
