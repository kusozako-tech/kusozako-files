
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .controllers.Controllers import EchoControllers


class DeltaStatusLabel(Gtk.Label, DeltaEntity):

    def _set_label(self):
        if self._selection_text is None:
            self.set_label(self._count_text)
        else:
            self.set_label(self._selection_text)

    def _delta_call_count_text_changed(self, text):
        self._count_text = text
        self._set_label()

    def _delta_call_selection_text_changed(self, text=None):
        self._selection_text = text
        self._set_label()

    def __init__(self, parent):
        self._parent = parent
        self._count_text = ""
        self._selection_text = None
        Gtk.Label.__init__(self, hexpand=True)
        EchoControllers(self)
        self._raise("delta > add to container", self)
