
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Transmitter import FoxtrotTransmitter
from kusozako_files import FileManagerColumnTypes
from kusozako_files import PopoverMenuSignals
from .PopupMenu import AlfaPopupMenu
from .media_file.MediaFile import DeltaMediaFile
from .copy_to_clipboard.CopyToClipboard import EchoCopyToClipboard


class AlfaPopupMenuForItem(AlfaPopupMenu):

    MODEL = "define popover model here"

    def _delta_info_gio_file(self):
        return self._gio_file

    def _delta_info_mime_starts_with(self, header):
        return self._media_file.starts_with(header)

    def _delta_call_select_from(self, signal):
        self._raise("delta > model signal", (signal, self._gio_file))

    def _delta_call_popover_menu_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_popover_menu_object(self, object_):
        self._transmitter.register_listener(object_)

    def _on_initialize(self):
        self._transmitter = FoxtrotTransmitter()
        self._media_file = DeltaMediaFile(self)
        EchoCopyToClipboard(self)

    def popup(self, tree_row, widget, x, y):
        self._gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        self._media_file.set_tree_row(tree_row)
        self._transmitter.transmit((PopoverMenuSignals.POPUP, None))
        self._popover_menu.popup_for_position(widget, x, y)
