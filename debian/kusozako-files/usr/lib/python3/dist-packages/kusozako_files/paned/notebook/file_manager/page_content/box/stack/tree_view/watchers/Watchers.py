
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .KeyPressEvent import DeltaKeyPressEvent
from .QueryTooltip import DeltaQueryTooltip
from .VAdjustment import DeltaVAdjustment
from .button_release_event.ButtonReleaseEvent import DeltaButtonReleaseEvent


class EchoWatchers:

    def __init__(self, parent):
        DeltaButtonReleaseEvent(parent)
        DeltaKeyPressEvent(parent)
        DeltaQueryTooltip(parent)
        DeltaVAdjustment(parent)
