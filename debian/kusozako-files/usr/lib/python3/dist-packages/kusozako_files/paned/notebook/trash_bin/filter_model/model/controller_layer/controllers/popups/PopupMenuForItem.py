
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk
from gi.repository import Gtk
from kusozako_files import FileManagerColumnTypes
from .PopupMenu import AlfaPopupMenu


class AlfaPopupMenuForItem(AlfaPopupMenu):

    MODEL = "define popover model here"

    def _delta_info_gio_file(self):
        return self._gio_file

    def _copy_to_clipboard(self, text):
        display = Gdk.Display.get_default()
        clipboard = Gtk.Clipboard.get_default(display)
        clipboard.set_text(text, -1)

    def _delta_call_copy_path(self):
        self._copy_to_clipboard(self._gio_file.get_path())

    def _delta_call_copy_uri(self):
        self._copy_to_clipboard(self._gio_file.get_uri())

    def _delta_call_select_from(self, signal):
        self._raise("delta > model signal", (signal, self._gio_file))

    def popup(self, tree_row, widget, x, y):
        self._gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        self._popover_menu.popup_for_position(widget, x, y)
