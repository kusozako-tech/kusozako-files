
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController


class DeltaFilterKeyword(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.FILTER_KEYWORD

    def _control(self, keyword):
        self._raise("delta > keyword changed", keyword)
        self._raise("delta > refilter")
