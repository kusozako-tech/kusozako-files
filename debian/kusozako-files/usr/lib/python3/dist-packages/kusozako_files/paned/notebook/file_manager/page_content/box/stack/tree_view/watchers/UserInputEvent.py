
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals


class DeltaUserInputEvent(DeltaEntity):

    EVENT = "define handling event here."

    def _try_activate(self, tree_row):
        param = FileManagerSignals.VIEWERS_ITEM_ACTIVATED, tree_row
        self._raise("delta > model signal", param)

    def _toggle_selection(self, tree_row):
        param = FileManagerSignals.SELECTION_TOGGLE, tree_row
        self._raise("delta > model signal", param)

    def _on_initialize(self, widget):
        pass

    def _on_event_dispatch(self, event, tree_row):
        raise NotImplementedError

    def _on_event(self, widget, event):
        tree_row = self._enquiry("delta > selected tree row")
        if tree_row is not None:
            self._event_dispatch(event, tree_row)

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.connect(self.EVENT, self._on_event)
        self._on_initialize(tree_view)
