
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import InterModelSignals

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "simple-action",
            "title": _("Open"),
            "message": "delta > callback",
            "user-data": InterModelSignals.FILE_MANAGER_MOVE_DIRECTORY,
            "close-on-clicked": True,
        },
        {
            "type": "simple-action",
            "title": _("Open in New Tab"),
            "message": "delta > callback",
            "user-data": InterModelSignals.FILE_MANAGER_NEW,
            "close-on-clicked": True,
        },
    ],
}

POPOVER_MODEL = [MAIN_PAGE]
