
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from kusozako_files import FilePermission
from .sub_controllers.SubControllers import EchoSubControllers


class DeltaPartSelection(DeltaEntity):

    def _has_permission(self, tree_row, check_permission):
        if not check_permission:
            return True
        # unreadable file can't be selected
        permission = tree_row[FileManagerColumnTypes.PERMISSION]
        return permission != FilePermission.UNREADABLE

    def _get_next(self, next_tree_row, check_permission):
        if next_tree_row is None:
            return False, None
        has_permission = self._has_permission(next_tree_row, check_permission)
        return has_permission, next_tree_row

    def _delta_info_tree_rows_from(self, user_data):
        tree_row, direction, check_permission = user_data
        while tree_row is not None:
            param = getattr(tree_row, direction), check_permission
            is_valid, tree_row = self._get_next(*param)
            if is_valid:
                yield tree_row

    def _delta_info_tree_row_for_gio_file(self, alfa_gio_file):
        filter_model = self._enquiry("delta > filter model")
        for tree_row in filter_model:
            bravo_gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
            if alfa_gio_file.equal(bravo_gio_file):
                return tree_row
        return None

    def __init__(self, parent):
        self._parent = parent
        EchoSubControllers(self)
