
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerColumnTypes
from .Selected import DeltaSelected
from .Icon import DeltaIcon
from .FileName import DeltaFileName
from .PropertyColumn import DeltaPropertyColumn
from .LastModified import DeltaLastModified


class EchoColumns:

    def __init__(self, parent):
        DeltaSelected(parent)
        DeltaIcon(parent)
        DeltaFileName(parent)
        DeltaPropertyColumn.new(
            parent,
            _("Permission"),
            FileManagerColumnTypes.PERMISSION_READABLE,
            FileManagerColumnTypes.PERMISSION
            )
        DeltaPropertyColumn.new(
            parent,
            _("File Size"),
            FileManagerColumnTypes.FILE_SIZE_READABLE,
            FileManagerColumnTypes.FILE_SIZE
            )
        DeltaPropertyColumn.new(
            parent,
            _("Mime Type"),
            FileManagerColumnTypes.CONTENT_TYPE,
            FileManagerColumnTypes.CONTENT_TYPE
            )
        DeltaLastModified(parent)
