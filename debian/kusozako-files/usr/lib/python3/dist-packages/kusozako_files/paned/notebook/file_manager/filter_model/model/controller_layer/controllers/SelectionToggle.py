
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals
from kusozako_files import FileManagerColumnTypes
from kusozako_files import FilePermission


class DeltaSelectionToggle(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SELECTION_TOGGLE

    def _has_permission(self, tree_row):
        # unreadable file can't be selected
        permission = tree_row[FileManagerColumnTypes.PERMISSION]
        return permission != FilePermission.UNREADABLE

    def _control(self, tree_row):
        if not self._has_permission(tree_row):
            return
        selected = tree_row[FileManagerColumnTypes.SELECTED]
        tree_row[FileManagerColumnTypes.SELECTED] = not selected
        param = FileManagerSignals.SELECTION_ACCEPTED, tree_row
        self._raise("delta > model signal", param)
