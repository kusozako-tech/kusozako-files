
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .CopyTo import DeltaCopyTo
from .MoveTo import DeltaMoveTo
from .Trash import DeltaTrash


class EchoOperations:

    def __init__(self, parent):
        DeltaCopyTo(parent)
        DeltaMoveTo(parent)
        DeltaTrash(parent)
