
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import InterModelSignals
from .PopupMenuForItem import AlfaPopupMenuForItem
from .popover_models.DirectoryPopoverMenuModels import MODEL


class DeltaPopupMenuForDirectory(AlfaPopupMenuForItem):

    MODEL = MODEL

    def _delta_call_open_in_new_tab(self):
        param = InterModelSignals.FILE_MANAGER_NEW, self._gio_file
        self._raise("delta > inter model signal", param)
