
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals
from kusozako_files import FileManagerColumnTypes
from kusozako_files.header_label.HeaderLabel import FoxtrotHeaderLabel

SHADE_COLOR = 255/255, 165/255, 0/255, 0.9


class DeltaSelectionAccepted(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SELECTION_ACCEPTED

    def _get_visible_pixbuf(self, tree_row):
        if not tree_row[FileManagerColumnTypes.SELECTED]:
            return tree_row[FileManagerColumnTypes.SOURCE_PIXBUF]
        pixbuf = tree_row[FileManagerColumnTypes.SOURCE_PIXBUF]
        return self._header_label.get_visible_pixbuf(pixbuf)

    def _set_selection_label(self, tree_row):
        pixbuf = self._get_visible_pixbuf(tree_row)
        tree_row[FileManagerColumnTypes.VISIBLE_PIXBUF] = pixbuf.copy()

    def _control(self, tree_row):
        self._set_selection_label(tree_row)
        gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        path = gio_file.get_path()
        if tree_row[FileManagerColumnTypes.SELECTED]:
            self._raise("delta > append", (tree_row, path))
        else:
            self._raise("delta > remove", path)

    def _on_initialize(self):
        self._header_label = FoxtrotHeaderLabel.new("SELECTED", SHADE_COLOR)
