
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .thumbnail_setter.ThumbnailSetter import FoxtrotThumbnailSetter
from .window.Window import DeltaWindow


class DeltaThumbnailChooser(DeltaEntity):

    @classmethod
    def run_for_gio_file(cls, parent, gio_file):
        thumbnail_chooser = cls(parent)
        return thumbnail_chooser.set_gio_file(gio_file)

    def _delta_call_tree_row_selected(self, tree_row):
        self._thumbnail_setter.apply(tree_row)
        self._window.response(Gtk.ResponseType.APPLY)

    def _delta_info_is_active(self):
        return self._is_active

    def _on_destroy(self, window):
        self._is_active = False

    def set_gio_file(self, gio_file):
        self._thumbnail_setter = FoxtrotThumbnailSetter(gio_file)
        self._window = DeltaWindow(self)
        self._window.connect("destroy", self._on_destroy)
        response = self._window.run()
        self._window.destroy()
        print("is active", self._window.props.is_active)
        return response

    def __init__(self, parent):
        self._parent = parent
        self._is_active = True
