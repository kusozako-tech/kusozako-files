
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from kusozako_files import FileManagerSignals
from kusozako_files import PopoverMenuSignals
from .thumbnail_chooser.ThumbnailChooser import DeltaThumbnailChooser


class DeltaMediaFile(DeltaEntity):

    def _action(self):
        gio_file = self._enquiry("delta > gio file")
        response = DeltaThumbnailChooser.run_for_gio_file(self, gio_file)
        if response == Gtk.ResponseType.APPLY:
            param = FileManagerSignals.MODEL_THUMBNAIL_CHANGED, gio_file
            self._raise("delta > model signal", param)

    def receive_transmission(self, user_data):
        signal, _ = user_data
        if signal != PopoverMenuSignals.REQUEST_CHANGE_THUMBNAIL:
            return
        self._action()

    def starts_with(self, header):
        return self._mime_type.startswith(header)

    def set_tree_row(self, tree_row):
        self._mime_type = tree_row[FileManagerColumnTypes.MIME_TYPE]

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register popover menu object", self)
