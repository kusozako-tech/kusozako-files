
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .RowActivated import DeltaRowActivated
from .ButtonReleaseEvent import DeltaButtonReleaseEvent


class EchoWatchers:

    def __init__(self, parent):
        DeltaRowActivated(parent)
        DeltaButtonReleaseEvent(parent)
