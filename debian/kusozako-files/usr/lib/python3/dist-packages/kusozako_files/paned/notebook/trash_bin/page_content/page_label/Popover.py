
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.popover_menu.PopoverMenu import DeltaPopoverMenu

MAIN_PAGE = {
    "page-name": "main",
    "items": [
        {
            "type": "simple-action",
            "title": _("Close Tab"),
            "message": "delta > close tab",
            "user-data": None,
            "close-on-clicked": True
        }
    ]
}

MODEL = [MAIN_PAGE]


class DeltaPopover(DeltaEntity):

    def _on_begin(self, gesture, sequence, popover):
        popover.popup_for_gesture(gesture)

    def __init__(self, parent):
        self._parent = parent
        popover = DeltaPopoverMenu.new_for_model(self, MODEL)
        event_source = self._enquiry("delta > event source")
        self._gesture_single = Gtk.GestureSingle(widget=event_source, button=3)
        self._gesture_single.connect("begin", self._on_begin, popover)
