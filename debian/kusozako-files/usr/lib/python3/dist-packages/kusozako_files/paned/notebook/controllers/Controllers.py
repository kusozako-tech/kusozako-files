
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from .FileManagerNew import DeltaFileManagerNew
from .FileManagerMoveDirectory import DeltaFileManagerMoveDirectory
from .TrashBinNew import DeltaTrashBinNew
from .StartupDirectories import DeltaStartupDirectories


class EchoControllers:

    def __init__(self, parent):
        DeltaFileManagerNew(parent)
        DeltaFileManagerMoveDirectory(parent)
        DeltaTrashBinNew(parent)
        DeltaStartupDirectories(parent)
