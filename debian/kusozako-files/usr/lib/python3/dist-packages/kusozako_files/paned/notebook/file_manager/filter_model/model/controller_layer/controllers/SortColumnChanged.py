
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals

A_TO_Z = Gtk.SortType.ASCENDING
Z_TO_A = Gtk.SortType.DESCENDING


class DeltaSortColumnChanged(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SORT_COLUMN_CHANGED

    def _control(self, column_id):
        model = self._enquiry("delta > model")
        current_column_id, current_order = model.get_sort_column_id()
        if current_column_id == column_id:
            new_order = A_TO_Z if current_order == Z_TO_A else Z_TO_A
        else:
            new_order = A_TO_Z
        model.set_sort_column_id(column_id, new_order)
        param = FileManagerSignals.VIEWERS_FORCE_RELOAD, None
        self._raise("delta > model signal", param)
