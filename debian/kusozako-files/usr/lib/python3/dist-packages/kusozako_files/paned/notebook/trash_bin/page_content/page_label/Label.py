
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

TRASH_DIR = GLib.build_filenamev([GLib.get_user_data_dir(), "Trash", "files"])


class DeltaLabel(DeltaEntity, Gtk.Label):

    def _get_label(self, gio_file=None):
        if gio_file is None:
            return "Trash:///"
        fullpath = gio_file.get_path()
        return fullpath.replace(TRASH_DIR, "Trash://")

    def receive_transmission(self, user_data):
        signal, gio_file = user_data
        if signal != FileManagerSignals.MODEL_MOVE_DIRECTORY:
            return
        label = self._get_label(gio_file)
        self.set_label(label)
        self.props.tooltip_text = label

    def __init__(self, parent):
        self._parent = parent
        Gtk.Label.__init__(self)
        self._raise("delta > add to container", self)
        self._raise("delta > register model object", self)
