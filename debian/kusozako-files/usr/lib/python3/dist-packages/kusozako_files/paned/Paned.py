
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Transmitter import FoxtrotTransmitter
from kusozako_files import Unit
from .side_pane.SidePane import DeltaSidePane
from .notebook.Notebook import DeltaNotebook


class DeltaPaned(Gtk.Paned, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add1(widget) if not self.get_children() else self.add2(widget)

    def _delta_call_inter_model_signal(self, user_data):
        self._transmitter.transmit(user_data)

    def _delta_call_register_inter_model_object(self, object_):
        self._transmitter.register_listener(object_)

    def __init__(self, parent):
        self._parent = parent
        self._transmitter = FoxtrotTransmitter()
        Gtk.Paned.__init__(self, wide_handle=True, position_set=True)
        query = "view", "side_pane_size", Unit.DEFAULT_SIDE_PANE_WIDTH
        side_pane_size = self._enquiry("delta > settings", query)
        self.set_position(side_pane_size)
        DeltaSidePane(self)
        DeltaNotebook(self)
        self._raise("delta > add to container", self)
