
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import InterModelSignals
from kusozako_files import BookmarkColumnTypes
from .columns.Columns import EchoColumns


class DeltaTreeView(Gtk.TreeView, DeltaEntity):

    def _on_row_activated(self, tree_view, tree_path, column):
        selection = self.get_selection()
        selection.unselect_all()
        param = InterModelSignals.TRASH_BIN_NEW, None
        self._raise("delta > inter model signal", param)

    def _delta_call_append_tree_view_column(self, column):
        self.append_column(column)

    def __init__(self, parent):
        self._parent = parent
        Gtk.TreeView.__init__(self, model=self._enquiry("delta > model"))
        self.set_headers_visible(False)
        self.set_tooltip_column(BookmarkColumnTypes.TOOLTIP_TEXT)
        EchoColumns(self)
        self.connect("row-activated", self._on_row_activated)
        self._raise("delta > css", (self, "secondary-theme-color-class"))
        self._raise("delta > add to container", self)
