
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import BookmarkColumnTypes
from .RowConstructor import BravoRowConstructor


class DeltaGtk3(DeltaEntity, BravoRowConstructor):

    def _try_append_row_data(self, gio_file):
        model = self._enquiry("delta > model")
        for tree_row in model:
            if gio_file.equal(tree_row[BookmarkColumnTypes.GIO_FILE]):
                return
        self._construct("gtk3", gio_file)

    def _read_line(self, line):
        items = line.split(" ")
        uri = items[0]
        gio_file = Gio.File.new_for_uri(uri)
        self._try_append_row_data(gio_file)

    def _read_path(self, path):
        gio_file = Gio.File.new_for_path(path)
        file_input_stream = gio_file.read()
        data_input_stream = Gio.DataInputStream.new(file_input_stream)
        while True:
            line, length = data_input_stream.read_line_utf8(None)
            if length == 0:
                break
            self._read_line(line)

    def __init__(self, parent):
        self._parent = parent
        names = GLib.get_user_config_dir(), "gtk-3.0", "bookmarks"
        path = GLib.build_filenamev(names)
        if GLib.file_test(path, GLib.FileTest.EXISTS):
            self._read_path(path)
