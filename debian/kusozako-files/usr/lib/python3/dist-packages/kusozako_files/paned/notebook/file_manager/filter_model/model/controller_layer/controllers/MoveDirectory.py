
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaMoveDirectory(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_MOVE_DIRECTORY

    def _control(self, gio_file):
        current_directory = self._enquiry("delta > current directory")
        if current_directory == gio_file.get_path():
            return
        self._raise("delta > move directory", gio_file)
