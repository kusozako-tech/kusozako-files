
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes

FORMAT = "\n\nSymbolic Link Target:\n\t{}"


class DeltaQueryTooltip(DeltaEntity):

    def _get_tooltip_text(self, tree_row):
        base_text = tree_row[FileManagerColumnTypes.TOOLTIP_TEXT]
        file_info = tree_row[FileManagerColumnTypes.FILE_INFO]
        if not file_info.get_is_symlink():
            return base_text
        symlink_target = file_info.get_symlink_target()
        gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
        target_file = gio_file.resolve_relative_path(symlink_target)
        base_text += FORMAT.format(target_file.get_path())
        return base_text

    def _get_tree_row(self, tree_view, x, y):
        _, border = tree_view.get_border()
        model = tree_view.get_model()
        try:
            tree_path, _, _, _ = tree_view.get_path_at_pos(x, y-border.top)
            return model[tree_path]
        except TypeError:
            # when pointed at header.
            return None

    def _on_query_tooltip(self, tree_view, x, y, keyboard_mode, tooltip):
        tree_row = self._get_tree_row(tree_view, x, y)
        if tree_row is None:
            return False
        tooltip.set_icon(tree_row[FileManagerColumnTypes.ORIGINAL_PIXBUF])
        tooltip.set_text(self._get_tooltip_text(tree_row))
        return True

    def __init__(self, parent):
        self._parent = parent
        tree_view = self._enquiry("delta > tree view")
        tree_view.props.has_tooltip = True
        tree_view.connect("query-tooltip", self._on_query_tooltip)
