
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes

TIMEUOT_INTERVAL = 10


class DeltaQueue(DeltaEntity):

    @classmethod
    def new_for_signal(cls, parent, signal):
        async_queue = cls(parent)
        async_queue.construct(signal)
        return async_queue

    def _try_get_gio_file(self, tree_row):
        try:
            gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
            return gio_file
        except TypeError:
            return None

    def _reset_store(self, param):
        self._store.clear()
        for tree_row in param.values():
            gio_file = self._try_get_gio_file(tree_row)
            if gio_file is not None:
                self._store.append(gio_file)

    def get_cache(self):
        return self._store.copy()

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self._signal:
            self._reset_store(param)

    def construct(self, signal):
        self._signal = signal
        self._raise("delta > register model object", self)

    def __init__(self, parent):
        self._parent = parent
        self._store = []
