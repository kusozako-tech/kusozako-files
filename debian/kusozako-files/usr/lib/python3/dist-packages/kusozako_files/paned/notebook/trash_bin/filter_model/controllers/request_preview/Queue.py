
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

import queue
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import PreviewStatus
from kusozako_files import FileManagerColumnTypes as ColumnTypes
from .CurrentDirectory import DeltaCurrentDirectory


class DeltaQueue(DeltaEntity):

    def _is_reload_needed(self, tree_row):
        directory = tree_row[ColumnTypes.DIRECTORY]
        if not self._current_directory.match(directory):
            return False
        if PreviewStatus.UNLOADED != tree_row[ColumnTypes.PREVIEW_STATUS]:
            return False
        return True

    def _set_queue_async(self, range_):
        tree_row_queue = queue.Queue()
        filter_model = self._enquiry("delta > filter model")
        base_model = filter_model.get_model()
        start_path, end_path = range_
        while start_path.compare(end_path) != 1:
            child_path = filter_model.convert_path_to_child_path(start_path)
            tree_row = base_model[child_path]
            if self._is_reload_needed(tree_row):
                tree_row[ColumnTypes.PREVIEW_STATUS] = PreviewStatus.PENDING
                tree_row_queue.put(tree_row)
            start_path.next()
        self._raise("delta > queue ready", tree_row_queue)

    def set_queue_async(self, range_):
        GLib.idle_add(self._set_queue_async, range_)

    def __init__(self, parent):
        self._parent = parent
        self._current_directory = DeltaCurrentDirectory(self)
