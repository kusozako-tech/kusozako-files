
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaModelLoaded(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.MODEL_LOADED

    def _control(self, param):
        self._raise("delta > reveal child", False)
