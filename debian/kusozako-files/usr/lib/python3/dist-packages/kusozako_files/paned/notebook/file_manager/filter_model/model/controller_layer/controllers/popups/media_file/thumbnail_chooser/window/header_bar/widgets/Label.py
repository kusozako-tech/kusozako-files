
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity


class DeltaLabel(Gtk.Label, DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        label = _("Select Video Thumbnail")
        Gtk.Label.__init__(self, label)
        self._raise("delta > pack center", self)
