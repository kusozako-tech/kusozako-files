
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from .model.Model import DeltaModel
from .keyword.Keyword import DeltaKeyword
from .controllers.Controllers import EchoControllers


class DeltaFilterModel(DeltaEntity):

    def _delta_call_refilter(self):
        # DO NOT RAISE FileManagerSignals.FILTER_REFILTERED HERE.
        # refiltering should take long time.
        # it should be raised by DeltaDelay.
        self._filter_model.refilter()

    def _delta_info_model(self):
        return self._model

    def _visible_func(self, model, tree_iter, base_model, user_data=None):
        tree_row = model[tree_iter]
        if tree_row[FileManagerColumnTypes.SELECTED]:
            return True
        return self._keyword.get_is_matched(tree_row)

    def __init__(self, parent):
        self._parent = parent
        self._keyword = DeltaKeyword(self)
        self._model = DeltaModel(self)
        self._filter_model = self._model.filter_new()
        self._filter_model.set_visible_func(self._visible_func, None)
        EchoControllers(self)

    @property
    def model(self):
        return self._filter_model
