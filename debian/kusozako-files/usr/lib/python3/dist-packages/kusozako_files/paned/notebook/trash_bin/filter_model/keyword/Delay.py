
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

DELAY = 100     # in milliseconds


class DeltaDelay(DeltaEntity):

    def _timeout(self, index):
        if index == self._index:
            signal_param = FileManagerSignals.FILTER_REFILTERED, None
            self._raise("delta > model signal", signal_param)
        return GLib.SOURCE_REMOVE

    def start(self):
        self._index += 1
        index = self._index
        GLib.timeout_add(DELAY, self._timeout, index)

    def __init__(self, parent):
        self._parent = parent
        self._index = 0
