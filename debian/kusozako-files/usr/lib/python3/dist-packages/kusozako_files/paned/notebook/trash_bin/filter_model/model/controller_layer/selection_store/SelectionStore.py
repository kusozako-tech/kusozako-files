
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .selections.Selections import EchoSelections
from .operations.Operations import EchoOperations
from .sub_controllers.SubControllers import EchoSubControllers


class DeltaSelectionStore(DeltaEntity):

    def _delta_call_append(self, user_data):
        self._selections.append(user_data)

    def _delta_call_remove(self, path):
        self._selections.remove(path)

    def _delta_call_clear(self):
        self._selections.clear()

    def __init__(self, parent):
        self._parent = parent
        self._selections = EchoSelections(self)
        EchoOperations(self)
        EchoSubControllers(self)
