
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes

TIMEUOT_INTERVAL = 10


class DeltaAsyncQueue(DeltaEntity):

    @classmethod
    def new_for_signal(cls, parent, signal):
        async_queue = cls(parent)
        async_queue.construct(signal)
        return async_queue

    def _timeout(self, gio_file_queue):
        if len(gio_file_queue) == 0:
            return GLib.SOURCE_REMOVE
        gio_file = gio_file_queue.pop()
        self._raise("delta > gio file queued", gio_file)
        return GLib.SOURCE_CONTINUE

    def start_queuing(self):
        gio_file_queue = self._store.copy()
        GLib.timeout_add(TIMEUOT_INTERVAL, self._timeout, gio_file_queue)

    def _try_get_gio_file(self, tree_row):
        try:
            gio_file = tree_row[FileManagerColumnTypes.GIO_FILE]
            return gio_file
        except TypeError:
            return None

    def _reset_store(self, param):
        self._store.clear()
        for tree_row in param.values():
            gio_file = self._try_get_gio_file(tree_row)
            if gio_file is None:
                continue
            self._store.append(gio_file)

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self._signal:
            self._reset_store(param)

    def construct(self, signal):
        self._signal = signal
        self._raise("delta > register model object", self)

    def __init__(self, parent):
        self._parent = parent
        self._store = []

    @property
    def has_queue(self):
        return len(self._store) > 0

    @property
    def selection(self):
        return self._store
