
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerSignals


class DeltaSortOrderChanged(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SORT_ORDER_CHANGED

    def _control(self, order):
        model = self._enquiry("delta > model")
        column_id, current_order = model.get_sort_column_id()
        if order == current_order:
            return
        model.set_sort_column_id(column_id, order)
        param = FileManagerSignals.VIEWERS_FORCE_RELOAD, None
        self._raise("delta > model signal", param)
