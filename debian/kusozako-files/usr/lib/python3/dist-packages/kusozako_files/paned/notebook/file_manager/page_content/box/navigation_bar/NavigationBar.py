
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import Unit
from .HomeButton import DeltaHomeButton
from .UpButton import DeltaUpButton
from .recent_button.RecentButton import DeltaRecentButton
from .address_entry.AddressEntry import DeltaAddressEntry
from .ToggleShowHiddenButton import DeltaToggleShowHiddenButton
from .ShowIconViewButton import DeltaShowIconViewButton
from .ShowTreeViewButton import DeltaShowTreeViewButton


class DeltaNavigationBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaHomeButton(self)
        DeltaUpButton(self)
        DeltaRecentButton(self)
        DeltaAddressEntry(self)
        DeltaToggleShowHiddenButton(self)
        DeltaShowIconViewButton(self)
        DeltaShowTreeViewButton(self)
        self.set_size_request(-1, Unit.TOOLBAR_HEIGHT)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
