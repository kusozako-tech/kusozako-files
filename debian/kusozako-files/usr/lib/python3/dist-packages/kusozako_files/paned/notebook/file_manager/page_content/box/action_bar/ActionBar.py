
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import Unit
from .status_label.StatusLabel import DeltaStatusLabel
from .CopyToButton import DeltaCopyToButton
from .MoveToButton import DeltaMoveToButton
from .TrashButton import DeltaTrashButton


class DeltaActionBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        DeltaStatusLabel(self)
        DeltaCopyToButton(self)
        DeltaMoveToButton(self)
        DeltaTrashButton(self)
        self.set_size_request(-1, Unit.TOOLBAR_HEIGHT)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
