
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals as Signals


SELECTION_OPERATIONS = (
    {
        "type": "simple-action",
        "title": _("Copy To ..."),
        "message": "delta > model signal",
        "user-data": (Signals.SELECTION_OPERATION_COPY_TO, None),
        "close-on-clicked": True
    },
    {
        "type": "simple-action",
        "title": _("Restore"),
        "message": "delta > model signal",
        "user-data": (Signals.SELECTION_OPERATION_RESTORE_TRASH, None),
        "close-on-clicked": True
    },
    {
        "type": "separator"
    },
    {
        "type": "simple-action",
        "title": _("Delete"),
        "message": "delta > model signal",
        "user-data": (Signals.SELECTION_OPERATION_DELETE_TRASH, None),
        "close-on-clicked": True
    }
)
