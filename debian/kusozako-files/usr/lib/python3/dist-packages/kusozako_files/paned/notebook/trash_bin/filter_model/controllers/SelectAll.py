
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerColumnTypes
from kusozako_files import FilePermission


class DeltaSelectAll(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SELECTION_SELECT_ALL

    def _has_permission(self, tree_row):
        # unreadable file can't be selected
        permission = tree_row[FileManagerColumnTypes.PERMISSION]
        return permission != FilePermission.UNREADABLE

    def _is_valid_selection(self, tree_row):
        if tree_row[FileManagerColumnTypes.SELECTED]:
            return False
        return self._has_permission(tree_row)

    def _queue_selection(self, new_selection):
        if not new_selection:
            return
        for tree_row in new_selection:
            param = FileManagerSignals.SELECTION_ACCEPTED, tree_row
            self._raise("delta > model signal", param)

    def _control(self, param=None):
        new_selection = []
        filter_model = self._enquiry("delta > filter model")
        for tree_row in filter_model:
            if not self._is_valid_selection(tree_row):
                continue
            tree_row[FileManagerColumnTypes.SELECTED] = True
            new_selection.append(tree_row)
        self._queue_selection(new_selection)
