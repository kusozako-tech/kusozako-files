
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity


class DeltaParser(DeltaEntity):

    def _get_row_data(self, trash_directory):
        trashes = tuple(trash_directory.enumerate_children("*", 0))
        length = len(trashes)
        return (
            Gio.Icon.new_for_string("user-trash-symbolic"),
            "trash-bin",
            "Trash Bin ({})".format(length),
            "{} items in trash bin".format(length),
            None,
            )

    def _on_changed(self, monitor, alfa, bravo, event, trash_directory):
        row_data = self._get_row_data(trash_directory)
        self._raise("delta > change row data", row_data)

    def __init__(self, parent):
        self._parent = parent
        trash_directory = Gio.File.new_for_uri("Trash:///")
        row_data = self._get_row_data(trash_directory)
        self._raise("delta > append row data", row_data)
        self._monitor = trash_directory.monitor_directory(
            Gio.FileMonitorFlags.NONE,
            None
            )
        self._monitor.connect("changed", self._on_changed, trash_directory)
