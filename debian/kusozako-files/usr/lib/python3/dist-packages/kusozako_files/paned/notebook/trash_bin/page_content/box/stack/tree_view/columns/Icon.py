
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_files import FileManagerColumnTypes


class DeltaIcon(Gtk.TreeViewColumn, DeltaEntity):

    def _data_func(self, column, renderer, model, tree_iter, user_data=None):
        renderer.set_property("height", Unit(4))
        tree_row = model[tree_iter]
        file_info = tree_row[FileManagerColumnTypes.FILE_INFO]
        gio_icon = file_info.get_symbolic_icon()
        renderer.set_property("gicon", gio_icon)
        color = "White" if tree_row.path[0] % 2 == 0 else "LightGrey"
        renderer.set_property("cell-background", color)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererPixbuf(xpad=Unit(1))
        Gtk.TreeViewColumn.__init__(
            self,
            title="",
            cell_renderer=renderer
            )
        self.set_expand(False)
        self.set_cell_data_func(renderer, self._data_func)
        self._raise("delta > append tree view column", self)
