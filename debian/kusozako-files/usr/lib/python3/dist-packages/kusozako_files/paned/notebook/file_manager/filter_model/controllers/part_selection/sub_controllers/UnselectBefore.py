
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from kusozako_files import FileManagerSignals
from kusozako_files.FileManagerController import AlfaFileManagerController
from kusozako_files import FileManagerColumnTypes


class DeltaUnselectBefore(AlfaFileManagerController):

    SIGNAL = FileManagerSignals.SELECTION_UNSELECT_BEFORE

    def _queue_selection(self, new_selection):
        for tree_row in new_selection:
            tree_row[FileManagerColumnTypes.SELECTED] = False
            param = FileManagerSignals.SELECTION_ACCEPTED, tree_row
            self._raise("delta > model signal", param)

    def _control(self, gio_file):
        target_row = self._enquiry("delta > tree row for gio file", gio_file)
        user_data = target_row, "previous", False
        new_selection = self._enquiry("delta > tree rows from", user_data)
        self._queue_selection(new_selection)
