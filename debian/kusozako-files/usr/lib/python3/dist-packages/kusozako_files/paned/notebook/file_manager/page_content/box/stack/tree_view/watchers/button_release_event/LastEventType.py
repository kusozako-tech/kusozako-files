
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gdk

ACTIVATE_EVENTS = Gdk.EventType._2BUTTON_PRESS, Gdk.EventType._3BUTTON_PRESS


class FoxtrotLastEventType:

    def is_selection_event(self):
        return self._last_event_type == Gdk.EventType.BUTTON_PRESS

    def is_activation_event(self):
        return self._last_event_type in ACTIVATE_EVENTS

    def _on_button_press(self, widget, event):
        self._last_event_type = event.type

    def __init__(self, widget):
        self._last_event_type = None
        widget.connect("button-press-event", self._on_button_press)
