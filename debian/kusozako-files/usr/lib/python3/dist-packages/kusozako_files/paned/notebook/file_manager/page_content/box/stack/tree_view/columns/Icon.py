
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from libkusozako3.Ux import Unit
from kusozako_files import FileManagerColumnTypes
from .ExtraFuncs import BravoExtraFuncs


class DeltaIcon(Gtk.TreeViewColumn, DeltaEntity, BravoExtraFuncs):

    def _data_func(self, column, renderer, model, tree_iter, user_data=None):
        tree_row = model[tree_iter]
        file_info = tree_row[FileManagerColumnTypes.FILE_INFO]
        gio_icon = file_info.get_icon()
        renderer.set_property("gicon", gio_icon)

    def __init__(self, parent):
        self._parent = parent
        renderer = Gtk.CellRendererPixbuf(xpad=Unit(1))
        Gtk.TreeViewColumn.__init__(
            self,
            cell_renderer=renderer
            )
        self.set_expand(False)
        self._set_stripe2(renderer)
        # self.set_cell_data_func(renderer, self._set_stripe)
        self._raise("delta > append tree view column", self)
