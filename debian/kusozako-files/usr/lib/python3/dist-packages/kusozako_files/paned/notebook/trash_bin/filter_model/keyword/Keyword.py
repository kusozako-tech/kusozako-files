
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerColumnTypes
from .controllers.Controllers import EchoControllers
from .Delay import DeltaDelay


class DeltaKeyword(DeltaEntity):

    def _delta_call_keyword_changed(self, keyword):
        self._keyword = str(keyword)

    def _delta_call_clear_keyword(self):
        self._keyword = None

    def get_is_matched(self, tree_row):
        if self._keyword is None:
            matched = True
        else:
            file_info = tree_row[FileManagerColumnTypes.FILE_INFO]
            matched = (self._keyword in file_info.get_name())
        self._delay.start()
        return matched

    def __init__(self, parent):
        self._parent = parent
        self._keyword = None
        self._delay = DeltaDelay(self)
        EchoControllers(self)
