
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from .bookmark.Bookmark import DeltaBokmark
from .previewers.Previewers import DeltaPreviewers


class DeltaSidePane(Gtk.Stack, DeltaEntity):

    def _delta_call_add_to_stack_named(self, user_data):
        widget, name = user_data
        self.add_named(widget, name)

    def _delta_call_switch_stack_to(self, name):
        self.set_visible_child_name(name)

    def _on_size_allocated(self, widget, rectangle):
        settings = "view", "side_pane_size", rectangle.width
        self._raise("delta > settings", settings)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Stack.__init__(
            self,
            transition_type=Gtk.StackTransitionType.OVER_RIGHT_LEFT
            )
        DeltaBokmark(self)
        DeltaPreviewers(self)
        self.connect("size-allocate", self._on_size_allocated)
        self._raise("delta > add to container", self)
