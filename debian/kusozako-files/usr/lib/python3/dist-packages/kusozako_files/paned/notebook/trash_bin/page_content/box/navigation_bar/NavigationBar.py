
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import Unit
from .UpButton import DeltaUpButton
from .SearchEntry import DeltaSearchEntry
from .ShowIconViewButton import DeltaShowIconViewButton
from .ShowTreeViewButton import DeltaShowTreeViewButton


class DeltaNavigationBar(Gtk.Box, DeltaEntity):

    def _delta_call_add_to_container(self, widget):
        self.add(widget)

    def __init__(self, parent):
        self._parent = parent
        Gtk.Box.__init__(self, orientation=Gtk.Orientation.HORIZONTAL)
        self.set_size_request(-1, Unit.TOOLBAR_HEIGHT)
        DeltaUpButton(self)
        DeltaSearchEntry(self)
        DeltaShowIconViewButton(self)
        DeltaShowTreeViewButton(self)
        self._raise("delta > add to container", self)
        self._raise("delta > css", (self, "primary-surface-color-class"))
