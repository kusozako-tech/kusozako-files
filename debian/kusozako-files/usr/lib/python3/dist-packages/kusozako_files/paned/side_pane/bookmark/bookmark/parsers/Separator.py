
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity


class DeltaSeparator(DeltaEntity):

    def __init__(self, parent):
        self._parent = parent
        dummy_gio_file = Gio.File.new_for_path("/dev/null")
        separator = None, "separator", "", "", dummy_gio_file
        self._raise("delta > append row data", separator)
