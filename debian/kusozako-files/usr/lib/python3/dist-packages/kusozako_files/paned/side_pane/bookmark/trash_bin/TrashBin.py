
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gtk
from libkusozako3.Entity import DeltaEntity
from kusozako_files import BookmarkColumnTypes
from .Parser import DeltaParser
from .TreeView import DeltaTreeView


class DeltaTrashBin(Gtk.ListStore, DeltaEntity):

    def _delta_info_model(self):
        return self

    def _delta_call_change_row_data(self, row_data):
        # trash bin tree view has only one row
        tree_row = self[0]
        for index in range(0, BookmarkColumnTypes.NUMBER_OF_COLUMNS):
            tree_row[index] = row_data[index]

    def _delta_call_append_row_data(self, row_data):
        self.append(row_data)

    def __init__(self, parent):
        self._parent = parent
        Gtk.ListStore.__init__(self, *BookmarkColumnTypes.TYPES)
        DeltaParser(self)
        DeltaTreeView(self)
