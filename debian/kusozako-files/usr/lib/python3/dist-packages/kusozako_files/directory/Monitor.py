
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository.Gio import FileMonitorEvent as EVENT
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

SIGNALS = {
    EVENT.CHANGED: FileManagerSignals.MONITOR_CHANGED,
    EVENT.CHANGES_DONE_HINT: FileManagerSignals.MONITOR_CHANGES_DONE,
    EVENT.DELETED: FileManagerSignals.MONITOR_DELETED,
    EVENT.CREATED: FileManagerSignals.MONITOR_CREATED,
    EVENT.ATTRIBUTE_CHANGED: FileManagerSignals.MONITOR_ATTRIBUTE_CHANGED,
    EVENT.RENAMED: FileManagerSignals.MONITOR_RENAMED,
    EVENT.MOVED_IN: FileManagerSignals.MONITOR_MOVED_IN,
    EVENT.MOVED_OUT: FileManagerSignals.MONITOR_MOVED_OUT
    }


class DeltaMonitor(DeltaEntity):

    def _on_changed(self, *args):
        file_monitor, alfa, bravo, event_type, directory = args
        signal = SIGNALS.get(event_type, None)
        if signal is None or alfa.get_path() == directory:
            return
        self._raise("delta > model signal", (signal, args))

    def set_gio_file(self, gio_file):
        if self._file_monitor is not None:
            self._file_monitor.cancel()
        self._file_monitor = gio_file.monitor_directory(
            Gio.FileMonitorFlags.WATCH_MOVES
            )
        directory = gio_file.get_path()
        self._file_monitor.connect("changed", self._on_changed, directory)

    def __init__(self, parent):
        self._parent = parent
        self._file_monitor = None
