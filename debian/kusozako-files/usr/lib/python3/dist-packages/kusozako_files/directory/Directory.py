
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity
from .CurrentDirectory import DeltaCurrentDirectory
from .Parser import DeltaParser
from .Monitor import DeltaMonitor


class DeltaDirectory(DeltaEntity):

    def set_gio_file(self, gio_file):
        self._current_directory.set_gio_file(gio_file)
        self._parser.set_gio_file(gio_file)
        self._monitor.set_gio_file(gio_file)

    def __init__(self, parent):
        self._parent = parent
        self._current_directory = DeltaCurrentDirectory(self)
        self._parser = DeltaParser(self)
        self._monitor = DeltaMonitor(self)

    @property
    def current_directory(self):
        return self._current_directory.path
