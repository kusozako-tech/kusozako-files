
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from libkusozako3.Entity import DeltaEntity
from gi.repository.Gio import FileMonitorEvent as EVENT
from kusozako_files import FileManagerSignals

SIGNALS = {
    EVENT.DELETED: FileManagerSignals.CURRENT_DIRECTORY_REMOVED,
    EVENT.RENAMED: FileManagerSignals.CURRENT_DIRECTORY_RENAMED,
    EVENT.MOVED_OUT: FileManagerSignals.CURRENT_DIRECTORY_REMOVED
    }


class DeltaCurrentDirectory(DeltaEntity):

    def _on_changed(self, *args):
        file_monitor, alfa, bravo, event_type, directory = args
        signal = SIGNALS.get(event_type, None)
        if signal is None:
            return
        self._raise("delta > model signal", (signal, args))

    def set_gio_file(self, gio_file):
        self._current_directory_gio_file = gio_file
        # instanciate file monitor as member for object continuous.
        self._file_monitor = gio_file.monitor_file(
            Gio.FileMonitorFlags.WATCH_MOVES
            )
        directory = gio_file.get_path()
        self._file_monitor.connect("changed", self._on_changed, directory)

    def __init__(self, parent):
        self._parent = parent
        self._current_directory_gio_file = None

    @property
    def path(self):
        if self._current_directory_gio_file is None:
            return None
        return self._current_directory_gio_file.get_path()
