

# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio
from gi.repository import GLib
from libkusozako3.Entity import DeltaEntity
from kusozako_files import FileManagerSignals

NUMBER_OF_FILES_AT_ONCE = 32


class DeltaParser(DeltaEntity):

    def _on_break(self, file_enumerator, gio_file):
        file_enumerator.close(None)
        param = FileManagerSignals.MODEL_LOADED, gio_file
        self._raise("delta > model signal", param)

    def _on_continue(self, file_enumerator, gio_file, file_infos):
        directory = gio_file.get_path()
        for file_info in file_infos:
            child_gio_file = file_enumerator.get_child(file_info)
            signal_param = directory, file_info, child_gio_file
            param = FileManagerSignals.MODEL_FILE_FOUND, signal_param
            self._raise("delta > model signal", param)
        self._enumerate(file_enumerator, gio_file)

    def _next_files_async_finished(self, file_enumerator, task, gio_file):
        file_infos = file_enumerator.next_files_finish(task)
        if not file_infos:
            self._on_break(file_enumerator, gio_file)
        else:
            self._on_continue(file_enumerator, gio_file, file_infos)

    def _enumerate(self, file_enumerator, gio_file):
        file_enumerator.next_files_async(
            NUMBER_OF_FILES_AT_ONCE,            # number of files to get
            GLib.PRIORITY_DEFAULT,              # priority
            None,                               # cancellable
            self._next_files_async_finished,    # gio async ready callback
            gio_file                            # data for callback
            )

    def _enumerate_children_async_finished(self, gio_file, task):
        file_enumerator = gio_file.enumerate_children_finish(task)
        self._enumerate(file_enumerator, gio_file)

    def set_gio_file(self, gio_file):
        gio_file.enumerate_children_async(
            "*",
            Gio.FileQueryInfoFlags.NONE,
            GLib.PRIORITY_DEFAULT,
            None,
            self._enumerate_children_async_finished
            )

    def __init__(self, parent):
        self._parent = parent
