
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from libkusozako3.Entity import DeltaEntity


class AlfaFileManagerController(DeltaEntity):

    SIGNAL = "define acceptable signal here"

    def _control(self, param):
        raise NotImplementedError

    def _on_initialize(self):
        # optional
        pass

    def receive_transmission(self, user_data):
        signal, param = user_data
        if signal == self.SIGNAL:
            self._control(param)

    def __init__(self, parent):
        self._parent = parent
        self._raise("delta > register model object", self)
        self._on_initialize()
