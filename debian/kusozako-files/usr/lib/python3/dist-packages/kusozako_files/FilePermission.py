
# (c) copyright 2021, takedanemuru <takeda.nemuru@yandex.com>
# SPDX-License-Identifier: GPL-3.0-or-later

from gi.repository import Gio

UNREADABLE = 0
READ_ONLY = 1
READ_WRITE = 2
EXECUTABLE_DIRECTORY = 3
EXECUTABLE_FILE = 4

CHECK_LIST = {
    "access::can-read": (UNREADABLE, _("Unreadable")),
    "access::can-write": (READ_ONLY, _("Read Only")),
    "access::can-execute": (READ_WRITE, _("Read-write")),
    }


def _get_permission_for_executable(file_info):
    if file_info.get_file_type() == Gio.FileType.DIRECTORY:
        return EXECUTABLE_DIRECTORY, _("Directory")
    return EXECUTABLE_FILE, _("Executable")


def get_permission(file_info):
    for key, value in CHECK_LIST.items():
        if not file_info.get_attribute_boolean(key):
            return value
    return _get_permission_for_executable(file_info)
